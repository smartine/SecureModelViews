# Secure Model Views

Secure Model Views is a fine-grained access-control mechanism for models. More specifically, is a mecanisms composed of: 

1. a role-based access-control language specially
designed to work with models allowing for the specification of conditions at the M2 and M1 level. 

2. an enforcement mechanism based on the automatic generation
of security compliant (virtual) views that protects both model and metamodel.

- PolicyDsl.xtext is the XText grammar for our policy language.
- Policy2View.atl is the main transformation module. It takes access-control policies and generates view specifications.

The following video demo shows how a generated view specification effectively creates a filtered metamodel (viewtype) and correspondingly filtered instance models (views) conforming to this viewtype.

![Demo Video](SecureViewsDemo.mp4)

