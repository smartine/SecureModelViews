/**
 * generated by Xtext 2.12.0
 */
package org.cea.lise.security.secureviews.policydsl.policyDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Decision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cea.lise.security.secureviews.policydsl.policyDsl.AccessDecision#getDecision <em>Decision</em>}</li>
 * </ul>
 *
 * @see org.cea.lise.security.secureviews.policydsl.policyDsl.PolicyDslPackage#getAccessDecision()
 * @model
 * @generated
 */
public interface AccessDecision extends EObject
{
  /**
   * Returns the value of the '<em><b>Decision</b></em>' attribute.
   * The literals are from the enumeration {@link org.cea.lise.security.secureviews.policydsl.policyDsl.DecisionKind}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Decision</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Decision</em>' attribute.
   * @see org.cea.lise.security.secureviews.policydsl.policyDsl.DecisionKind
   * @see #setDecision(DecisionKind)
   * @see org.cea.lise.security.secureviews.policydsl.policyDsl.PolicyDslPackage#getAccessDecision_Decision()
   * @model
   * @generated
   */
  DecisionKind getDecision();

  /**
   * Sets the value of the '{@link org.cea.lise.security.secureviews.policydsl.policyDsl.AccessDecision#getDecision <em>Decision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Decision</em>' attribute.
   * @see org.cea.lise.security.secureviews.policydsl.policyDsl.DecisionKind
   * @see #getDecision()
   * @generated
   */
  void setDecision(DecisionKind value);

} // AccessDecision
