/*
 * generated by Xtext 2.12.0
 */
grammar InternalPolicyDsl;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package org.cea.lise.security.secureviews.policydsl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package org.cea.lise.security.secureviews.policydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.cea.lise.security.secureviews.policydsl.services.PolicyDslGrammarAccess;

}

@parser::members {

 	private PolicyDslGrammarAccess grammarAccess;

    public InternalPolicyDslParser(TokenStream input, PolicyDslGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "Policy";
   	}

   	@Override
   	protected PolicyDslGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRulePolicy
entryRulePolicy returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getPolicyRule()); }
	iv_rulePolicy=rulePolicy
	{ $current=$iv_rulePolicy.current; }
	EOF;

// Rule Policy
rulePolicy returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				{
					newCompositeNode(grammarAccess.getPolicyAccess().getImportsImportParserRuleCall_0_0());
				}
				lv_imports_0_0=ruleImport
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getPolicyRule());
					}
					set(
						$current,
						"imports",
						lv_imports_0_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Import");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getPolicyAccess().getRoledeclarationRoleDeclarationParserRuleCall_1_0());
				}
				lv_roledeclaration_1_0=ruleRoleDeclaration
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getPolicyRule());
					}
					add(
						$current,
						"roledeclaration",
						lv_roledeclaration_1_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.RoleDeclaration");
					afterParserOrEnumRuleCall();
				}
			)
		)*
		(
			(
				{
					newCompositeNode(grammarAccess.getPolicyAccess().getRulesRuleParserRuleCall_2_0());
				}
				lv_rules_2_0=ruleRule
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getPolicyRule());
					}
					add(
						$current,
						"rules",
						lv_rules_2_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Rule");
					afterParserOrEnumRuleCall();
				}
			)
		)*
	)
;

// Entry rule entryRuleRoleDeclaration
entryRuleRoleDeclaration returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getRoleDeclarationRule()); }
	iv_ruleRoleDeclaration=ruleRoleDeclaration
	{ $current=$iv_ruleRoleDeclaration.current; }
	EOF;

// Rule RoleDeclaration
ruleRoleDeclaration returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='DeclareRole'
		{
			newLeafNode(otherlv_0, grammarAccess.getRoleDeclarationAccess().getDeclareRoleKeyword_0());
		}
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getRoleDeclarationAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getRoleDeclarationRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
	)
;

// Entry rule entryRuleImport
entryRuleImport returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getImportRule()); }
	iv_ruleImport=ruleImport
	{ $current=$iv_ruleImport.current; }
	EOF;

// Rule Import
ruleImport returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='import'
		{
			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
		}
		(
			(
				lv_importURI_1_0=RULE_STRING
				{
					newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getImportRule());
					}
					setWithLastConsumed(
						$current,
						"importURI",
						lv_importURI_1_0,
						"org.eclipse.xtext.common.Terminals.STRING");
				}
			)
		)
	)
;

// Entry rule entryRuleRule
entryRuleRule returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getRuleRule()); }
	iv_ruleRule=ruleRule
	{ $current=$iv_ruleRule.current; }
	EOF;

// Rule Rule
ruleRule returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='rule'
		{
			newLeafNode(otherlv_0, grammarAccess.getRuleAccess().getRuleKeyword_0());
		}
		(
			(
				lv_id_1_0=RULE_ID
				{
					newLeafNode(lv_id_1_0, grammarAccess.getRuleAccess().getIdIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getRuleRule());
					}
					setWithLastConsumed(
						$current,
						"id",
						lv_id_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2='('
		{
			newLeafNode(otherlv_2, grammarAccess.getRuleAccess().getLeftParenthesisKeyword_2());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getRuleAccess().getLhsLHSParserRuleCall_3_0());
				}
				lv_lhs_3_0=ruleLHS
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getRuleRule());
					}
					set(
						$current,
						"lhs",
						lv_lhs_3_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.LHS");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_4=')'
		{
			newLeafNode(otherlv_4, grammarAccess.getRuleAccess().getRightParenthesisKeyword_4());
		}
		otherlv_5='->'
		{
			newLeafNode(otherlv_5, grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_5());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getRuleAccess().getRhsRHSParserRuleCall_6_0());
				}
				lv_rhs_6_0=ruleRHS
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getRuleRule());
					}
					set(
						$current,
						"rhs",
						lv_rhs_6_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.RHS");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleLHS
entryRuleLHS returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getLHSRule()); }
	iv_ruleLHS=ruleLHS
	{ $current=$iv_ruleLHS.current; }
	EOF;

// Rule LHS
ruleLHS returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='Subject'
		{
			newLeafNode(otherlv_0, grammarAccess.getLHSAccess().getSubjectKeyword_0());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_1_0());
				}
				lv_subjects_1_0=ruleSubject
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getLHSRule());
					}
					add(
						$current,
						"subjects",
						lv_subjects_1_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Subject");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			otherlv_2=','
			{
				newLeafNode(otherlv_2, grammarAccess.getLHSAccess().getCommaKeyword_2_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_2_1_0());
					}
					lv_subjects_3_0=ruleSubject
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getLHSRule());
						}
						add(
							$current,
							"subjects",
							lv_subjects_3_0,
							"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Subject");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
		otherlv_4=';'
		{
			newLeafNode(otherlv_4, grammarAccess.getLHSAccess().getSemicolonKeyword_3());
		}
		otherlv_5='Action'
		{
			newLeafNode(otherlv_5, grammarAccess.getLHSAccess().getActionKeyword_4());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_5_0());
				}
				lv_actions_6_0=ruleAction
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getLHSRule());
					}
					add(
						$current,
						"actions",
						lv_actions_6_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Action");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			otherlv_7=','
			{
				newLeafNode(otherlv_7, grammarAccess.getLHSAccess().getCommaKeyword_6_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_6_1_0());
					}
					lv_actions_8_0=ruleAction
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getLHSRule());
						}
						add(
							$current,
							"actions",
							lv_actions_8_0,
							"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Action");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
		otherlv_9=';'
		{
			newLeafNode(otherlv_9, grammarAccess.getLHSAccess().getSemicolonKeyword_7());
		}
		otherlv_10='Object'
		{
			newLeafNode(otherlv_10, grammarAccess.getLHSAccess().getObjectKeyword_8());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getLHSAccess().getObjectConditionObjectConditionParserRuleCall_9_0());
				}
				lv_objectCondition_11_0=ruleObjectCondition
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getLHSRule());
					}
					set(
						$current,
						"objectCondition",
						lv_objectCondition_11_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.ObjectCondition");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleRHS
entryRuleRHS returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getRHSRule()); }
	iv_ruleRHS=ruleRHS
	{ $current=$iv_ruleRHS.current; }
	EOF;

// Rule RHS
ruleRHS returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				newCompositeNode(grammarAccess.getRHSAccess().getDecisionsAccessDecisionParserRuleCall_0());
			}
			lv_decisions_0_0=ruleAccessDecision
			{
				if ($current==null) {
					$current = createModelElementForParent(grammarAccess.getRHSRule());
				}
				add(
					$current,
					"decisions",
					lv_decisions_0_0,
					"org.cea.lise.security.secureviews.policydsl.PolicyDsl.AccessDecision");
				afterParserOrEnumRuleCall();
			}
		)
	)
;

// Entry rule entryRuleSubject
entryRuleSubject returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSubjectRule()); }
	iv_ruleSubject=ruleSubject
	{ $current=$iv_ruleSubject.current; }
	EOF;

// Rule Subject
ruleSubject returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSubjectAccess().getSubjectAction_0(),
					$current);
			}
		)
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSubjectRule());
					}
				}
				otherlv_1=RULE_ID
				{
					newLeafNode(otherlv_1, grammarAccess.getSubjectAccess().getNameRoleDeclarationCrossReference_1_0());
				}
			)
		)
	)
;

// Entry rule entryRuleAction
entryRuleAction returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getActionRule()); }
	iv_ruleAction=ruleAction
	{ $current=$iv_ruleAction.current; }
	EOF;

// Rule Action
ruleAction returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getActionAccess().getActionAction_0(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getActionAccess().getActionActionKindEnumRuleCall_1_0());
				}
				lv_action_1_0=ruleActionKind
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getActionRule());
					}
					set(
						$current,
						"action",
						lv_action_1_0,
						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.ActionKind");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleObjectCondition
entryRuleObjectCondition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getObjectConditionRule()); }
	iv_ruleObjectCondition=ruleObjectCondition
	{ $current=$iv_ruleObjectCondition.current; }
	EOF;

// Rule ObjectCondition
ruleObjectCondition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getObjectConditionAccess().getClassConditionParserRuleCall_0());
		}
		this_ClassCondition_0=ruleClassCondition
		{
			$current = $this_ClassCondition_0.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getObjectConditionAccess().getFeatureConditionParserRuleCall_1());
		}
		this_FeatureCondition_1=ruleFeatureCondition
		{
			$current = $this_FeatureCondition_1.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getObjectConditionAccess().getReferenceConditionParserRuleCall_2());
		}
		this_ReferenceCondition_2=ruleReferenceCondition
		{
			$current = $this_ReferenceCondition_2.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getObjectConditionAccess().getOperationConditionParserRuleCall_3());
		}
		this_OperationCondition_3=ruleOperationCondition
		{
			$current = $this_OperationCondition_3.current;
			afterParserOrEnumRuleCall();
		}
	)
;

// Entry rule entryRuleClassCondition
entryRuleClassCondition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getClassConditionRule()); }
	iv_ruleClassCondition=ruleClassCondition
	{ $current=$iv_ruleClassCondition.current; }
	EOF;

// Rule ClassCondition
ruleClassCondition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='class'
		{
			newLeafNode(otherlv_0, grammarAccess.getClassConditionAccess().getClassKeyword_0());
		}
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getClassConditionRule());
					}
				}
				{
					newCompositeNode(grammarAccess.getClassConditionAccess().getClassCondRefEClassCrossReference_1_0());
				}
				ruleFQN
				{
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			otherlv_2='WithValue'
			{
				newLeafNode(otherlv_2, grammarAccess.getClassConditionAccess().getWithValueKeyword_2_0());
			}
			otherlv_3='='
			{
				newLeafNode(otherlv_3, grammarAccess.getClassConditionAccess().getEqualsSignKeyword_2_1());
			}
			otherlv_4='<'
			{
				newLeafNode(otherlv_4, grammarAccess.getClassConditionAccess().getLessThanSignKeyword_2_2());
			}
			(
				(
					lv_value_5_0=RULE_STRING
					{
						newLeafNode(lv_value_5_0, grammarAccess.getClassConditionAccess().getValueSTRINGTerminalRuleCall_2_3_0());
					}
					{
						if ($current==null) {
							$current = createModelElement(grammarAccess.getClassConditionRule());
						}
						setWithLastConsumed(
							$current,
							"value",
							lv_value_5_0,
							"org.eclipse.xtext.common.Terminals.STRING");
					}
				)
			)
			otherlv_6='>'
			{
				newLeafNode(otherlv_6, grammarAccess.getClassConditionAccess().getGreaterThanSignKeyword_2_4());
			}
		)?
	)
;

// Entry rule entryRuleReferenceCondition
entryRuleReferenceCondition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getReferenceConditionRule()); }
	iv_ruleReferenceCondition=ruleReferenceCondition
	{ $current=$iv_ruleReferenceCondition.current; }
	EOF;

// Rule ReferenceCondition
ruleReferenceCondition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='ref'
		{
			newLeafNode(otherlv_0, grammarAccess.getReferenceConditionAccess().getRefKeyword_0());
		}
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getReferenceConditionRule());
					}
				}
				{
					newCompositeNode(grammarAccess.getReferenceConditionAccess().getFeatureCondRefEReferenceCrossReference_1_0());
				}
				ruleFQN
				{
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleFeatureCondition
entryRuleFeatureCondition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getFeatureConditionRule()); }
	iv_ruleFeatureCondition=ruleFeatureCondition
	{ $current=$iv_ruleFeatureCondition.current; }
	EOF;

// Rule FeatureCondition
ruleFeatureCondition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='att'
		{
			newLeafNode(otherlv_0, grammarAccess.getFeatureConditionAccess().getAttKeyword_0());
		}
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getFeatureConditionRule());
					}
				}
				{
					newCompositeNode(grammarAccess.getFeatureConditionAccess().getFeatureCondRefEAttributeCrossReference_1_0());
				}
				ruleFQN
				{
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleOperationCondition
entryRuleOperationCondition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getOperationConditionRule()); }
	iv_ruleOperationCondition=ruleOperationCondition
	{ $current=$iv_ruleOperationCondition.current; }
	EOF;

// Rule OperationCondition
ruleOperationCondition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='op'
		{
			newLeafNode(otherlv_0, grammarAccess.getOperationConditionAccess().getOpKeyword_0());
		}
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getOperationConditionRule());
					}
				}
				{
					newCompositeNode(grammarAccess.getOperationConditionAccess().getOperationCondRefEOperationCrossReference_1_0());
				}
				ruleFQN
				{
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleAccessDecision
entryRuleAccessDecision returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getAccessDecisionRule()); }
	iv_ruleAccessDecision=ruleAccessDecision
	{ $current=$iv_ruleAccessDecision.current; }
	EOF;

// Rule AccessDecision
ruleAccessDecision returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				newCompositeNode(grammarAccess.getAccessDecisionAccess().getDecisionDecisionKindEnumRuleCall_0());
			}
			lv_decision_0_0=ruleDecisionKind
			{
				if ($current==null) {
					$current = createModelElementForParent(grammarAccess.getAccessDecisionRule());
				}
				set(
					$current,
					"decision",
					lv_decision_0_0,
					"org.cea.lise.security.secureviews.policydsl.PolicyDsl.DecisionKind");
				afterParserOrEnumRuleCall();
			}
		)
	)
;

// Entry rule entryRuleFQN
entryRuleFQN returns [String current=null]:
	{ newCompositeNode(grammarAccess.getFQNRule()); }
	iv_ruleFQN=ruleFQN
	{ $current=$iv_ruleFQN.current.getText(); }
	EOF;

// Rule FQN
ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		this_ID_0=RULE_ID
		{
			$current.merge(this_ID_0);
		}
		{
			newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0());
		}
		(
			kw='.'
			{
				$current.merge(kw);
				newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0());
			}
			this_ID_2=RULE_ID
			{
				$current.merge(this_ID_2);
			}
			{
				newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1());
			}
		)*
	)
;

// Rule ActionKind
ruleActionKind returns [Enumerator current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			enumLiteral_0='Read'
			{
				$current = grammarAccess.getActionKindAccess().getReadEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
				newLeafNode(enumLiteral_0, grammarAccess.getActionKindAccess().getReadEnumLiteralDeclaration_0());
			}
		)
		    |
		(
			enumLiteral_1='Write'
			{
				$current = grammarAccess.getActionKindAccess().getWriteEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
				newLeafNode(enumLiteral_1, grammarAccess.getActionKindAccess().getWriteEnumLiteralDeclaration_1());
			}
		)
		    |
		(
			enumLiteral_2='Execute'
			{
				$current = grammarAccess.getActionKindAccess().getExecuteEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
				newLeafNode(enumLiteral_2, grammarAccess.getActionKindAccess().getExecuteEnumLiteralDeclaration_2());
			}
		)
	)
;

// Rule DecisionKind
ruleDecisionKind returns [Enumerator current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			enumLiteral_0='Accept'
			{
				$current = grammarAccess.getDecisionKindAccess().getAcceptEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
				newLeafNode(enumLiteral_0, grammarAccess.getDecisionKindAccess().getAcceptEnumLiteralDeclaration_0());
			}
		)
		    |
		(
			enumLiteral_1='Deny'
			{
				$current = grammarAccess.getDecisionKindAccess().getDenyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
				newLeafNode(enumLiteral_1, grammarAccess.getDecisionKindAccess().getDenyEnumLiteralDeclaration_1());
			}
		)
		    |
		(
			enumLiteral_2='Undetermined'
			{
				$current = grammarAccess.getDecisionKindAccess().getUndeterminedEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
				newLeafNode(enumLiteral_2, grammarAccess.getDecisionKindAccess().getUndeterminedEnumLiteralDeclaration_2());
			}
		)
	)
;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
