package org.cea.lise.security.secureviews.policydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.cea.lise.security.secureviews.policydsl.services.PolicyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPolicyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'DeclareRole'", "'import'", "'rule'", "'('", "')'", "'->'", "'Subject'", "','", "';'", "'Action'", "'Object'", "'class'", "'WithValue'", "'='", "'<'", "'>'", "'ref'", "'att'", "'op'", "'.'", "'Read'", "'Write'", "'Execute'", "'Accept'", "'Deny'", "'Undetermined'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPolicyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPolicyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPolicyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPolicyDsl.g"; }



     	private PolicyDslGrammarAccess grammarAccess;

        public InternalPolicyDslParser(TokenStream input, PolicyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Policy";
       	}

       	@Override
       	protected PolicyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePolicy"
    // InternalPolicyDsl.g:65:1: entryRulePolicy returns [EObject current=null] : iv_rulePolicy= rulePolicy EOF ;
    public final EObject entryRulePolicy() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePolicy = null;


        try {
            // InternalPolicyDsl.g:65:47: (iv_rulePolicy= rulePolicy EOF )
            // InternalPolicyDsl.g:66:2: iv_rulePolicy= rulePolicy EOF
            {
             newCompositeNode(grammarAccess.getPolicyRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePolicy=rulePolicy();

            state._fsp--;

             current =iv_rulePolicy; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePolicy"


    // $ANTLR start "rulePolicy"
    // InternalPolicyDsl.g:72:1: rulePolicy returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) ) ( (lv_roledeclaration_1_0= ruleRoleDeclaration ) )* ( (lv_rules_2_0= ruleRule ) )* ) ;
    public final EObject rulePolicy() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_0_0 = null;

        EObject lv_roledeclaration_1_0 = null;

        EObject lv_rules_2_0 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:78:2: ( ( ( (lv_imports_0_0= ruleImport ) ) ( (lv_roledeclaration_1_0= ruleRoleDeclaration ) )* ( (lv_rules_2_0= ruleRule ) )* ) )
            // InternalPolicyDsl.g:79:2: ( ( (lv_imports_0_0= ruleImport ) ) ( (lv_roledeclaration_1_0= ruleRoleDeclaration ) )* ( (lv_rules_2_0= ruleRule ) )* )
            {
            // InternalPolicyDsl.g:79:2: ( ( (lv_imports_0_0= ruleImport ) ) ( (lv_roledeclaration_1_0= ruleRoleDeclaration ) )* ( (lv_rules_2_0= ruleRule ) )* )
            // InternalPolicyDsl.g:80:3: ( (lv_imports_0_0= ruleImport ) ) ( (lv_roledeclaration_1_0= ruleRoleDeclaration ) )* ( (lv_rules_2_0= ruleRule ) )*
            {
            // InternalPolicyDsl.g:80:3: ( (lv_imports_0_0= ruleImport ) )
            // InternalPolicyDsl.g:81:4: (lv_imports_0_0= ruleImport )
            {
            // InternalPolicyDsl.g:81:4: (lv_imports_0_0= ruleImport )
            // InternalPolicyDsl.g:82:5: lv_imports_0_0= ruleImport
            {

            					newCompositeNode(grammarAccess.getPolicyAccess().getImportsImportParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_3);
            lv_imports_0_0=ruleImport();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPolicyRule());
            					}
            					set(
            						current,
            						"imports",
            						lv_imports_0_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Import");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPolicyDsl.g:99:3: ( (lv_roledeclaration_1_0= ruleRoleDeclaration ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPolicyDsl.g:100:4: (lv_roledeclaration_1_0= ruleRoleDeclaration )
            	    {
            	    // InternalPolicyDsl.g:100:4: (lv_roledeclaration_1_0= ruleRoleDeclaration )
            	    // InternalPolicyDsl.g:101:5: lv_roledeclaration_1_0= ruleRoleDeclaration
            	    {

            	    					newCompositeNode(grammarAccess.getPolicyAccess().getRoledeclarationRoleDeclarationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_roledeclaration_1_0=ruleRoleDeclaration();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPolicyRule());
            	    					}
            	    					add(
            	    						current,
            	    						"roledeclaration",
            	    						lv_roledeclaration_1_0,
            	    						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.RoleDeclaration");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalPolicyDsl.g:118:3: ( (lv_rules_2_0= ruleRule ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPolicyDsl.g:119:4: (lv_rules_2_0= ruleRule )
            	    {
            	    // InternalPolicyDsl.g:119:4: (lv_rules_2_0= ruleRule )
            	    // InternalPolicyDsl.g:120:5: lv_rules_2_0= ruleRule
            	    {

            	    					newCompositeNode(grammarAccess.getPolicyAccess().getRulesRuleParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_rules_2_0=ruleRule();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPolicyRule());
            	    					}
            	    					add(
            	    						current,
            	    						"rules",
            	    						lv_rules_2_0,
            	    						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Rule");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePolicy"


    // $ANTLR start "entryRuleRoleDeclaration"
    // InternalPolicyDsl.g:141:1: entryRuleRoleDeclaration returns [EObject current=null] : iv_ruleRoleDeclaration= ruleRoleDeclaration EOF ;
    public final EObject entryRuleRoleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleDeclaration = null;


        try {
            // InternalPolicyDsl.g:141:56: (iv_ruleRoleDeclaration= ruleRoleDeclaration EOF )
            // InternalPolicyDsl.g:142:2: iv_ruleRoleDeclaration= ruleRoleDeclaration EOF
            {
             newCompositeNode(grammarAccess.getRoleDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRoleDeclaration=ruleRoleDeclaration();

            state._fsp--;

             current =iv_ruleRoleDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleDeclaration"


    // $ANTLR start "ruleRoleDeclaration"
    // InternalPolicyDsl.g:148:1: ruleRoleDeclaration returns [EObject current=null] : (otherlv_0= 'DeclareRole' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleRoleDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:154:2: ( (otherlv_0= 'DeclareRole' ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalPolicyDsl.g:155:2: (otherlv_0= 'DeclareRole' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalPolicyDsl.g:155:2: (otherlv_0= 'DeclareRole' ( (lv_name_1_0= RULE_ID ) ) )
            // InternalPolicyDsl.g:156:3: otherlv_0= 'DeclareRole' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getRoleDeclarationAccess().getDeclareRoleKeyword_0());
            		
            // InternalPolicyDsl.g:160:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalPolicyDsl.g:161:4: (lv_name_1_0= RULE_ID )
            {
            // InternalPolicyDsl.g:161:4: (lv_name_1_0= RULE_ID )
            // InternalPolicyDsl.g:162:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRoleDeclarationAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRoleDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleDeclaration"


    // $ANTLR start "entryRuleImport"
    // InternalPolicyDsl.g:182:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalPolicyDsl.g:182:47: (iv_ruleImport= ruleImport EOF )
            // InternalPolicyDsl.g:183:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalPolicyDsl.g:189:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:195:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalPolicyDsl.g:196:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalPolicyDsl.g:196:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalPolicyDsl.g:197:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalPolicyDsl.g:201:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalPolicyDsl.g:202:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalPolicyDsl.g:202:4: (lv_importURI_1_0= RULE_STRING )
            // InternalPolicyDsl.g:203:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportRule());
            					}
            					setWithLastConsumed(
            						current,
            						"importURI",
            						lv_importURI_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleRule"
    // InternalPolicyDsl.g:223:1: entryRuleRule returns [EObject current=null] : iv_ruleRule= ruleRule EOF ;
    public final EObject entryRuleRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRule = null;


        try {
            // InternalPolicyDsl.g:223:45: (iv_ruleRule= ruleRule EOF )
            // InternalPolicyDsl.g:224:2: iv_ruleRule= ruleRule EOF
            {
             newCompositeNode(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRule=ruleRule();

            state._fsp--;

             current =iv_ruleRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // InternalPolicyDsl.g:230:1: ruleRule returns [EObject current=null] : (otherlv_0= 'rule' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_lhs_3_0= ruleLHS ) ) otherlv_4= ')' otherlv_5= '->' ( (lv_rhs_6_0= ruleRHS ) ) ) ;
    public final EObject ruleRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_lhs_3_0 = null;

        EObject lv_rhs_6_0 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:236:2: ( (otherlv_0= 'rule' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_lhs_3_0= ruleLHS ) ) otherlv_4= ')' otherlv_5= '->' ( (lv_rhs_6_0= ruleRHS ) ) ) )
            // InternalPolicyDsl.g:237:2: (otherlv_0= 'rule' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_lhs_3_0= ruleLHS ) ) otherlv_4= ')' otherlv_5= '->' ( (lv_rhs_6_0= ruleRHS ) ) )
            {
            // InternalPolicyDsl.g:237:2: (otherlv_0= 'rule' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_lhs_3_0= ruleLHS ) ) otherlv_4= ')' otherlv_5= '->' ( (lv_rhs_6_0= ruleRHS ) ) )
            // InternalPolicyDsl.g:238:3: otherlv_0= 'rule' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_lhs_3_0= ruleLHS ) ) otherlv_4= ')' otherlv_5= '->' ( (lv_rhs_6_0= ruleRHS ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getRuleAccess().getRuleKeyword_0());
            		
            // InternalPolicyDsl.g:242:3: ( (lv_id_1_0= RULE_ID ) )
            // InternalPolicyDsl.g:243:4: (lv_id_1_0= RULE_ID )
            {
            // InternalPolicyDsl.g:243:4: (lv_id_1_0= RULE_ID )
            // InternalPolicyDsl.g:244:5: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_id_1_0, grammarAccess.getRuleAccess().getIdIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRuleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getRuleAccess().getLeftParenthesisKeyword_2());
            		
            // InternalPolicyDsl.g:264:3: ( (lv_lhs_3_0= ruleLHS ) )
            // InternalPolicyDsl.g:265:4: (lv_lhs_3_0= ruleLHS )
            {
            // InternalPolicyDsl.g:265:4: (lv_lhs_3_0= ruleLHS )
            // InternalPolicyDsl.g:266:5: lv_lhs_3_0= ruleLHS
            {

            					newCompositeNode(grammarAccess.getRuleAccess().getLhsLHSParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_9);
            lv_lhs_3_0=ruleLHS();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRuleRule());
            					}
            					set(
            						current,
            						"lhs",
            						lv_lhs_3_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.LHS");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_4, grammarAccess.getRuleAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,16,FOLLOW_11); 

            			newLeafNode(otherlv_5, grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_5());
            		
            // InternalPolicyDsl.g:291:3: ( (lv_rhs_6_0= ruleRHS ) )
            // InternalPolicyDsl.g:292:4: (lv_rhs_6_0= ruleRHS )
            {
            // InternalPolicyDsl.g:292:4: (lv_rhs_6_0= ruleRHS )
            // InternalPolicyDsl.g:293:5: lv_rhs_6_0= ruleRHS
            {

            					newCompositeNode(grammarAccess.getRuleAccess().getRhsRHSParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_2);
            lv_rhs_6_0=ruleRHS();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRuleRule());
            					}
            					set(
            						current,
            						"rhs",
            						lv_rhs_6_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.RHS");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRuleLHS"
    // InternalPolicyDsl.g:314:1: entryRuleLHS returns [EObject current=null] : iv_ruleLHS= ruleLHS EOF ;
    public final EObject entryRuleLHS() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLHS = null;


        try {
            // InternalPolicyDsl.g:314:44: (iv_ruleLHS= ruleLHS EOF )
            // InternalPolicyDsl.g:315:2: iv_ruleLHS= ruleLHS EOF
            {
             newCompositeNode(grammarAccess.getLHSRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLHS=ruleLHS();

            state._fsp--;

             current =iv_ruleLHS; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLHS"


    // $ANTLR start "ruleLHS"
    // InternalPolicyDsl.g:321:1: ruleLHS returns [EObject current=null] : (otherlv_0= 'Subject' ( (lv_subjects_1_0= ruleSubject ) ) (otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) ) )* otherlv_4= ';' otherlv_5= 'Action' ( (lv_actions_6_0= ruleAction ) ) (otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) ) )* otherlv_9= ';' otherlv_10= 'Object' ( (lv_objectCondition_11_0= ruleObjectCondition ) ) ) ;
    public final EObject ruleLHS() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_subjects_1_0 = null;

        EObject lv_subjects_3_0 = null;

        EObject lv_actions_6_0 = null;

        EObject lv_actions_8_0 = null;

        EObject lv_objectCondition_11_0 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:327:2: ( (otherlv_0= 'Subject' ( (lv_subjects_1_0= ruleSubject ) ) (otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) ) )* otherlv_4= ';' otherlv_5= 'Action' ( (lv_actions_6_0= ruleAction ) ) (otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) ) )* otherlv_9= ';' otherlv_10= 'Object' ( (lv_objectCondition_11_0= ruleObjectCondition ) ) ) )
            // InternalPolicyDsl.g:328:2: (otherlv_0= 'Subject' ( (lv_subjects_1_0= ruleSubject ) ) (otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) ) )* otherlv_4= ';' otherlv_5= 'Action' ( (lv_actions_6_0= ruleAction ) ) (otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) ) )* otherlv_9= ';' otherlv_10= 'Object' ( (lv_objectCondition_11_0= ruleObjectCondition ) ) )
            {
            // InternalPolicyDsl.g:328:2: (otherlv_0= 'Subject' ( (lv_subjects_1_0= ruleSubject ) ) (otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) ) )* otherlv_4= ';' otherlv_5= 'Action' ( (lv_actions_6_0= ruleAction ) ) (otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) ) )* otherlv_9= ';' otherlv_10= 'Object' ( (lv_objectCondition_11_0= ruleObjectCondition ) ) )
            // InternalPolicyDsl.g:329:3: otherlv_0= 'Subject' ( (lv_subjects_1_0= ruleSubject ) ) (otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) ) )* otherlv_4= ';' otherlv_5= 'Action' ( (lv_actions_6_0= ruleAction ) ) (otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) ) )* otherlv_9= ';' otherlv_10= 'Object' ( (lv_objectCondition_11_0= ruleObjectCondition ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getLHSAccess().getSubjectKeyword_0());
            		
            // InternalPolicyDsl.g:333:3: ( (lv_subjects_1_0= ruleSubject ) )
            // InternalPolicyDsl.g:334:4: (lv_subjects_1_0= ruleSubject )
            {
            // InternalPolicyDsl.g:334:4: (lv_subjects_1_0= ruleSubject )
            // InternalPolicyDsl.g:335:5: lv_subjects_1_0= ruleSubject
            {

            					newCompositeNode(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_12);
            lv_subjects_1_0=ruleSubject();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLHSRule());
            					}
            					add(
            						current,
            						"subjects",
            						lv_subjects_1_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Subject");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPolicyDsl.g:352:3: (otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==18) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalPolicyDsl.g:353:4: otherlv_2= ',' ( (lv_subjects_3_0= ruleSubject ) )
            	    {
            	    otherlv_2=(Token)match(input,18,FOLLOW_5); 

            	    				newLeafNode(otherlv_2, grammarAccess.getLHSAccess().getCommaKeyword_2_0());
            	    			
            	    // InternalPolicyDsl.g:357:4: ( (lv_subjects_3_0= ruleSubject ) )
            	    // InternalPolicyDsl.g:358:5: (lv_subjects_3_0= ruleSubject )
            	    {
            	    // InternalPolicyDsl.g:358:5: (lv_subjects_3_0= ruleSubject )
            	    // InternalPolicyDsl.g:359:6: lv_subjects_3_0= ruleSubject
            	    {

            	    						newCompositeNode(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_2_1_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_subjects_3_0=ruleSubject();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLHSRule());
            	    						}
            	    						add(
            	    							current,
            	    							"subjects",
            	    							lv_subjects_3_0,
            	    							"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Subject");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_4=(Token)match(input,19,FOLLOW_13); 

            			newLeafNode(otherlv_4, grammarAccess.getLHSAccess().getSemicolonKeyword_3());
            		
            otherlv_5=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_5, grammarAccess.getLHSAccess().getActionKeyword_4());
            		
            // InternalPolicyDsl.g:385:3: ( (lv_actions_6_0= ruleAction ) )
            // InternalPolicyDsl.g:386:4: (lv_actions_6_0= ruleAction )
            {
            // InternalPolicyDsl.g:386:4: (lv_actions_6_0= ruleAction )
            // InternalPolicyDsl.g:387:5: lv_actions_6_0= ruleAction
            {

            					newCompositeNode(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_12);
            lv_actions_6_0=ruleAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLHSRule());
            					}
            					add(
            						current,
            						"actions",
            						lv_actions_6_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Action");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPolicyDsl.g:404:3: (otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==18) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPolicyDsl.g:405:4: otherlv_7= ',' ( (lv_actions_8_0= ruleAction ) )
            	    {
            	    otherlv_7=(Token)match(input,18,FOLLOW_14); 

            	    				newLeafNode(otherlv_7, grammarAccess.getLHSAccess().getCommaKeyword_6_0());
            	    			
            	    // InternalPolicyDsl.g:409:4: ( (lv_actions_8_0= ruleAction ) )
            	    // InternalPolicyDsl.g:410:5: (lv_actions_8_0= ruleAction )
            	    {
            	    // InternalPolicyDsl.g:410:5: (lv_actions_8_0= ruleAction )
            	    // InternalPolicyDsl.g:411:6: lv_actions_8_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_actions_8_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLHSRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_8_0,
            	    							"org.cea.lise.security.secureviews.policydsl.PolicyDsl.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_9=(Token)match(input,19,FOLLOW_15); 

            			newLeafNode(otherlv_9, grammarAccess.getLHSAccess().getSemicolonKeyword_7());
            		
            otherlv_10=(Token)match(input,21,FOLLOW_16); 

            			newLeafNode(otherlv_10, grammarAccess.getLHSAccess().getObjectKeyword_8());
            		
            // InternalPolicyDsl.g:437:3: ( (lv_objectCondition_11_0= ruleObjectCondition ) )
            // InternalPolicyDsl.g:438:4: (lv_objectCondition_11_0= ruleObjectCondition )
            {
            // InternalPolicyDsl.g:438:4: (lv_objectCondition_11_0= ruleObjectCondition )
            // InternalPolicyDsl.g:439:5: lv_objectCondition_11_0= ruleObjectCondition
            {

            					newCompositeNode(grammarAccess.getLHSAccess().getObjectConditionObjectConditionParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_2);
            lv_objectCondition_11_0=ruleObjectCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLHSRule());
            					}
            					set(
            						current,
            						"objectCondition",
            						lv_objectCondition_11_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.ObjectCondition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLHS"


    // $ANTLR start "entryRuleRHS"
    // InternalPolicyDsl.g:460:1: entryRuleRHS returns [EObject current=null] : iv_ruleRHS= ruleRHS EOF ;
    public final EObject entryRuleRHS() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRHS = null;


        try {
            // InternalPolicyDsl.g:460:44: (iv_ruleRHS= ruleRHS EOF )
            // InternalPolicyDsl.g:461:2: iv_ruleRHS= ruleRHS EOF
            {
             newCompositeNode(grammarAccess.getRHSRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRHS=ruleRHS();

            state._fsp--;

             current =iv_ruleRHS; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRHS"


    // $ANTLR start "ruleRHS"
    // InternalPolicyDsl.g:467:1: ruleRHS returns [EObject current=null] : ( (lv_decisions_0_0= ruleAccessDecision ) ) ;
    public final EObject ruleRHS() throws RecognitionException {
        EObject current = null;

        EObject lv_decisions_0_0 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:473:2: ( ( (lv_decisions_0_0= ruleAccessDecision ) ) )
            // InternalPolicyDsl.g:474:2: ( (lv_decisions_0_0= ruleAccessDecision ) )
            {
            // InternalPolicyDsl.g:474:2: ( (lv_decisions_0_0= ruleAccessDecision ) )
            // InternalPolicyDsl.g:475:3: (lv_decisions_0_0= ruleAccessDecision )
            {
            // InternalPolicyDsl.g:475:3: (lv_decisions_0_0= ruleAccessDecision )
            // InternalPolicyDsl.g:476:4: lv_decisions_0_0= ruleAccessDecision
            {

            				newCompositeNode(grammarAccess.getRHSAccess().getDecisionsAccessDecisionParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_decisions_0_0=ruleAccessDecision();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getRHSRule());
            				}
            				add(
            					current,
            					"decisions",
            					lv_decisions_0_0,
            					"org.cea.lise.security.secureviews.policydsl.PolicyDsl.AccessDecision");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRHS"


    // $ANTLR start "entryRuleSubject"
    // InternalPolicyDsl.g:496:1: entryRuleSubject returns [EObject current=null] : iv_ruleSubject= ruleSubject EOF ;
    public final EObject entryRuleSubject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubject = null;


        try {
            // InternalPolicyDsl.g:496:48: (iv_ruleSubject= ruleSubject EOF )
            // InternalPolicyDsl.g:497:2: iv_ruleSubject= ruleSubject EOF
            {
             newCompositeNode(grammarAccess.getSubjectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSubject=ruleSubject();

            state._fsp--;

             current =iv_ruleSubject; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubject"


    // $ANTLR start "ruleSubject"
    // InternalPolicyDsl.g:503:1: ruleSubject returns [EObject current=null] : ( () ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleSubject() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:509:2: ( ( () ( (otherlv_1= RULE_ID ) ) ) )
            // InternalPolicyDsl.g:510:2: ( () ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalPolicyDsl.g:510:2: ( () ( (otherlv_1= RULE_ID ) ) )
            // InternalPolicyDsl.g:511:3: () ( (otherlv_1= RULE_ID ) )
            {
            // InternalPolicyDsl.g:511:3: ()
            // InternalPolicyDsl.g:512:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSubjectAccess().getSubjectAction_0(),
            					current);
            			

            }

            // InternalPolicyDsl.g:518:3: ( (otherlv_1= RULE_ID ) )
            // InternalPolicyDsl.g:519:4: (otherlv_1= RULE_ID )
            {
            // InternalPolicyDsl.g:519:4: (otherlv_1= RULE_ID )
            // InternalPolicyDsl.g:520:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSubjectRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getSubjectAccess().getNameRoleDeclarationCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubject"


    // $ANTLR start "entryRuleAction"
    // InternalPolicyDsl.g:535:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalPolicyDsl.g:535:47: (iv_ruleAction= ruleAction EOF )
            // InternalPolicyDsl.g:536:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalPolicyDsl.g:542:1: ruleAction returns [EObject current=null] : ( () ( (lv_action_1_0= ruleActionKind ) ) ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Enumerator lv_action_1_0 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:548:2: ( ( () ( (lv_action_1_0= ruleActionKind ) ) ) )
            // InternalPolicyDsl.g:549:2: ( () ( (lv_action_1_0= ruleActionKind ) ) )
            {
            // InternalPolicyDsl.g:549:2: ( () ( (lv_action_1_0= ruleActionKind ) ) )
            // InternalPolicyDsl.g:550:3: () ( (lv_action_1_0= ruleActionKind ) )
            {
            // InternalPolicyDsl.g:550:3: ()
            // InternalPolicyDsl.g:551:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionAccess().getActionAction_0(),
            					current);
            			

            }

            // InternalPolicyDsl.g:557:3: ( (lv_action_1_0= ruleActionKind ) )
            // InternalPolicyDsl.g:558:4: (lv_action_1_0= ruleActionKind )
            {
            // InternalPolicyDsl.g:558:4: (lv_action_1_0= ruleActionKind )
            // InternalPolicyDsl.g:559:5: lv_action_1_0= ruleActionKind
            {

            					newCompositeNode(grammarAccess.getActionAccess().getActionActionKindEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_action_1_0=ruleActionKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"action",
            						lv_action_1_0,
            						"org.cea.lise.security.secureviews.policydsl.PolicyDsl.ActionKind");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleObjectCondition"
    // InternalPolicyDsl.g:580:1: entryRuleObjectCondition returns [EObject current=null] : iv_ruleObjectCondition= ruleObjectCondition EOF ;
    public final EObject entryRuleObjectCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectCondition = null;


        try {
            // InternalPolicyDsl.g:580:56: (iv_ruleObjectCondition= ruleObjectCondition EOF )
            // InternalPolicyDsl.g:581:2: iv_ruleObjectCondition= ruleObjectCondition EOF
            {
             newCompositeNode(grammarAccess.getObjectConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjectCondition=ruleObjectCondition();

            state._fsp--;

             current =iv_ruleObjectCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectCondition"


    // $ANTLR start "ruleObjectCondition"
    // InternalPolicyDsl.g:587:1: ruleObjectCondition returns [EObject current=null] : (this_ClassCondition_0= ruleClassCondition | this_FeatureCondition_1= ruleFeatureCondition | this_ReferenceCondition_2= ruleReferenceCondition | this_OperationCondition_3= ruleOperationCondition ) ;
    public final EObject ruleObjectCondition() throws RecognitionException {
        EObject current = null;

        EObject this_ClassCondition_0 = null;

        EObject this_FeatureCondition_1 = null;

        EObject this_ReferenceCondition_2 = null;

        EObject this_OperationCondition_3 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:593:2: ( (this_ClassCondition_0= ruleClassCondition | this_FeatureCondition_1= ruleFeatureCondition | this_ReferenceCondition_2= ruleReferenceCondition | this_OperationCondition_3= ruleOperationCondition ) )
            // InternalPolicyDsl.g:594:2: (this_ClassCondition_0= ruleClassCondition | this_FeatureCondition_1= ruleFeatureCondition | this_ReferenceCondition_2= ruleReferenceCondition | this_OperationCondition_3= ruleOperationCondition )
            {
            // InternalPolicyDsl.g:594:2: (this_ClassCondition_0= ruleClassCondition | this_FeatureCondition_1= ruleFeatureCondition | this_ReferenceCondition_2= ruleReferenceCondition | this_OperationCondition_3= ruleOperationCondition )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt5=1;
                }
                break;
            case 28:
                {
                alt5=2;
                }
                break;
            case 27:
                {
                alt5=3;
                }
                break;
            case 29:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalPolicyDsl.g:595:3: this_ClassCondition_0= ruleClassCondition
                    {

                    			newCompositeNode(grammarAccess.getObjectConditionAccess().getClassConditionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ClassCondition_0=ruleClassCondition();

                    state._fsp--;


                    			current = this_ClassCondition_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPolicyDsl.g:604:3: this_FeatureCondition_1= ruleFeatureCondition
                    {

                    			newCompositeNode(grammarAccess.getObjectConditionAccess().getFeatureConditionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_FeatureCondition_1=ruleFeatureCondition();

                    state._fsp--;


                    			current = this_FeatureCondition_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPolicyDsl.g:613:3: this_ReferenceCondition_2= ruleReferenceCondition
                    {

                    			newCompositeNode(grammarAccess.getObjectConditionAccess().getReferenceConditionParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReferenceCondition_2=ruleReferenceCondition();

                    state._fsp--;


                    			current = this_ReferenceCondition_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalPolicyDsl.g:622:3: this_OperationCondition_3= ruleOperationCondition
                    {

                    			newCompositeNode(grammarAccess.getObjectConditionAccess().getOperationConditionParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_OperationCondition_3=ruleOperationCondition();

                    state._fsp--;


                    			current = this_OperationCondition_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectCondition"


    // $ANTLR start "entryRuleClassCondition"
    // InternalPolicyDsl.g:634:1: entryRuleClassCondition returns [EObject current=null] : iv_ruleClassCondition= ruleClassCondition EOF ;
    public final EObject entryRuleClassCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassCondition = null;


        try {
            // InternalPolicyDsl.g:634:55: (iv_ruleClassCondition= ruleClassCondition EOF )
            // InternalPolicyDsl.g:635:2: iv_ruleClassCondition= ruleClassCondition EOF
            {
             newCompositeNode(grammarAccess.getClassConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassCondition=ruleClassCondition();

            state._fsp--;

             current =iv_ruleClassCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassCondition"


    // $ANTLR start "ruleClassCondition"
    // InternalPolicyDsl.g:641:1: ruleClassCondition returns [EObject current=null] : (otherlv_0= 'class' ( ( ruleFQN ) ) (otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>' )? ) ;
    public final EObject ruleClassCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:647:2: ( (otherlv_0= 'class' ( ( ruleFQN ) ) (otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>' )? ) )
            // InternalPolicyDsl.g:648:2: (otherlv_0= 'class' ( ( ruleFQN ) ) (otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>' )? )
            {
            // InternalPolicyDsl.g:648:2: (otherlv_0= 'class' ( ( ruleFQN ) ) (otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>' )? )
            // InternalPolicyDsl.g:649:3: otherlv_0= 'class' ( ( ruleFQN ) ) (otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>' )?
            {
            otherlv_0=(Token)match(input,22,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getClassConditionAccess().getClassKeyword_0());
            		
            // InternalPolicyDsl.g:653:3: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:654:4: ( ruleFQN )
            {
            // InternalPolicyDsl.g:654:4: ( ruleFQN )
            // InternalPolicyDsl.g:655:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getClassConditionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getClassConditionAccess().getClassCondRefEClassCrossReference_1_0());
            				
            pushFollow(FOLLOW_17);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPolicyDsl.g:669:3: (otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==23) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalPolicyDsl.g:670:4: otherlv_2= 'WithValue' otherlv_3= '=' otherlv_4= '<' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= '>'
                    {
                    otherlv_2=(Token)match(input,23,FOLLOW_18); 

                    				newLeafNode(otherlv_2, grammarAccess.getClassConditionAccess().getWithValueKeyword_2_0());
                    			
                    otherlv_3=(Token)match(input,24,FOLLOW_19); 

                    				newLeafNode(otherlv_3, grammarAccess.getClassConditionAccess().getEqualsSignKeyword_2_1());
                    			
                    otherlv_4=(Token)match(input,25,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getClassConditionAccess().getLessThanSignKeyword_2_2());
                    			
                    // InternalPolicyDsl.g:682:4: ( (lv_value_5_0= RULE_STRING ) )
                    // InternalPolicyDsl.g:683:5: (lv_value_5_0= RULE_STRING )
                    {
                    // InternalPolicyDsl.g:683:5: (lv_value_5_0= RULE_STRING )
                    // InternalPolicyDsl.g:684:6: lv_value_5_0= RULE_STRING
                    {
                    lv_value_5_0=(Token)match(input,RULE_STRING,FOLLOW_20); 

                    						newLeafNode(lv_value_5_0, grammarAccess.getClassConditionAccess().getValueSTRINGTerminalRuleCall_2_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getClassConditionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,26,FOLLOW_2); 

                    				newLeafNode(otherlv_6, grammarAccess.getClassConditionAccess().getGreaterThanSignKeyword_2_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassCondition"


    // $ANTLR start "entryRuleReferenceCondition"
    // InternalPolicyDsl.g:709:1: entryRuleReferenceCondition returns [EObject current=null] : iv_ruleReferenceCondition= ruleReferenceCondition EOF ;
    public final EObject entryRuleReferenceCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceCondition = null;


        try {
            // InternalPolicyDsl.g:709:59: (iv_ruleReferenceCondition= ruleReferenceCondition EOF )
            // InternalPolicyDsl.g:710:2: iv_ruleReferenceCondition= ruleReferenceCondition EOF
            {
             newCompositeNode(grammarAccess.getReferenceConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReferenceCondition=ruleReferenceCondition();

            state._fsp--;

             current =iv_ruleReferenceCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceCondition"


    // $ANTLR start "ruleReferenceCondition"
    // InternalPolicyDsl.g:716:1: ruleReferenceCondition returns [EObject current=null] : (otherlv_0= 'ref' ( ( ruleFQN ) ) ) ;
    public final EObject ruleReferenceCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:722:2: ( (otherlv_0= 'ref' ( ( ruleFQN ) ) ) )
            // InternalPolicyDsl.g:723:2: (otherlv_0= 'ref' ( ( ruleFQN ) ) )
            {
            // InternalPolicyDsl.g:723:2: (otherlv_0= 'ref' ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:724:3: otherlv_0= 'ref' ( ( ruleFQN ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getReferenceConditionAccess().getRefKeyword_0());
            		
            // InternalPolicyDsl.g:728:3: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:729:4: ( ruleFQN )
            {
            // InternalPolicyDsl.g:729:4: ( ruleFQN )
            // InternalPolicyDsl.g:730:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReferenceConditionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getReferenceConditionAccess().getFeatureCondRefEReferenceCrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceCondition"


    // $ANTLR start "entryRuleFeatureCondition"
    // InternalPolicyDsl.g:748:1: entryRuleFeatureCondition returns [EObject current=null] : iv_ruleFeatureCondition= ruleFeatureCondition EOF ;
    public final EObject entryRuleFeatureCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureCondition = null;


        try {
            // InternalPolicyDsl.g:748:57: (iv_ruleFeatureCondition= ruleFeatureCondition EOF )
            // InternalPolicyDsl.g:749:2: iv_ruleFeatureCondition= ruleFeatureCondition EOF
            {
             newCompositeNode(grammarAccess.getFeatureConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureCondition=ruleFeatureCondition();

            state._fsp--;

             current =iv_ruleFeatureCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureCondition"


    // $ANTLR start "ruleFeatureCondition"
    // InternalPolicyDsl.g:755:1: ruleFeatureCondition returns [EObject current=null] : (otherlv_0= 'att' ( ( ruleFQN ) ) ) ;
    public final EObject ruleFeatureCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:761:2: ( (otherlv_0= 'att' ( ( ruleFQN ) ) ) )
            // InternalPolicyDsl.g:762:2: (otherlv_0= 'att' ( ( ruleFQN ) ) )
            {
            // InternalPolicyDsl.g:762:2: (otherlv_0= 'att' ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:763:3: otherlv_0= 'att' ( ( ruleFQN ) )
            {
            otherlv_0=(Token)match(input,28,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureConditionAccess().getAttKeyword_0());
            		
            // InternalPolicyDsl.g:767:3: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:768:4: ( ruleFQN )
            {
            // InternalPolicyDsl.g:768:4: ( ruleFQN )
            // InternalPolicyDsl.g:769:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureConditionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getFeatureConditionAccess().getFeatureCondRefEAttributeCrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureCondition"


    // $ANTLR start "entryRuleOperationCondition"
    // InternalPolicyDsl.g:787:1: entryRuleOperationCondition returns [EObject current=null] : iv_ruleOperationCondition= ruleOperationCondition EOF ;
    public final EObject entryRuleOperationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperationCondition = null;


        try {
            // InternalPolicyDsl.g:787:59: (iv_ruleOperationCondition= ruleOperationCondition EOF )
            // InternalPolicyDsl.g:788:2: iv_ruleOperationCondition= ruleOperationCondition EOF
            {
             newCompositeNode(grammarAccess.getOperationConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperationCondition=ruleOperationCondition();

            state._fsp--;

             current =iv_ruleOperationCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperationCondition"


    // $ANTLR start "ruleOperationCondition"
    // InternalPolicyDsl.g:794:1: ruleOperationCondition returns [EObject current=null] : (otherlv_0= 'op' ( ( ruleFQN ) ) ) ;
    public final EObject ruleOperationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:800:2: ( (otherlv_0= 'op' ( ( ruleFQN ) ) ) )
            // InternalPolicyDsl.g:801:2: (otherlv_0= 'op' ( ( ruleFQN ) ) )
            {
            // InternalPolicyDsl.g:801:2: (otherlv_0= 'op' ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:802:3: otherlv_0= 'op' ( ( ruleFQN ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getOperationConditionAccess().getOpKeyword_0());
            		
            // InternalPolicyDsl.g:806:3: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:807:4: ( ruleFQN )
            {
            // InternalPolicyDsl.g:807:4: ( ruleFQN )
            // InternalPolicyDsl.g:808:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOperationConditionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getOperationConditionAccess().getOperationCondRefEOperationCrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperationCondition"


    // $ANTLR start "entryRuleAccessDecision"
    // InternalPolicyDsl.g:826:1: entryRuleAccessDecision returns [EObject current=null] : iv_ruleAccessDecision= ruleAccessDecision EOF ;
    public final EObject entryRuleAccessDecision() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAccessDecision = null;


        try {
            // InternalPolicyDsl.g:826:55: (iv_ruleAccessDecision= ruleAccessDecision EOF )
            // InternalPolicyDsl.g:827:2: iv_ruleAccessDecision= ruleAccessDecision EOF
            {
             newCompositeNode(grammarAccess.getAccessDecisionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAccessDecision=ruleAccessDecision();

            state._fsp--;

             current =iv_ruleAccessDecision; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAccessDecision"


    // $ANTLR start "ruleAccessDecision"
    // InternalPolicyDsl.g:833:1: ruleAccessDecision returns [EObject current=null] : ( (lv_decision_0_0= ruleDecisionKind ) ) ;
    public final EObject ruleAccessDecision() throws RecognitionException {
        EObject current = null;

        Enumerator lv_decision_0_0 = null;



        	enterRule();

        try {
            // InternalPolicyDsl.g:839:2: ( ( (lv_decision_0_0= ruleDecisionKind ) ) )
            // InternalPolicyDsl.g:840:2: ( (lv_decision_0_0= ruleDecisionKind ) )
            {
            // InternalPolicyDsl.g:840:2: ( (lv_decision_0_0= ruleDecisionKind ) )
            // InternalPolicyDsl.g:841:3: (lv_decision_0_0= ruleDecisionKind )
            {
            // InternalPolicyDsl.g:841:3: (lv_decision_0_0= ruleDecisionKind )
            // InternalPolicyDsl.g:842:4: lv_decision_0_0= ruleDecisionKind
            {

            				newCompositeNode(grammarAccess.getAccessDecisionAccess().getDecisionDecisionKindEnumRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_decision_0_0=ruleDecisionKind();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getAccessDecisionRule());
            				}
            				set(
            					current,
            					"decision",
            					lv_decision_0_0,
            					"org.cea.lise.security.secureviews.policydsl.PolicyDsl.DecisionKind");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAccessDecision"


    // $ANTLR start "entryRuleFQN"
    // InternalPolicyDsl.g:862:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalPolicyDsl.g:862:43: (iv_ruleFQN= ruleFQN EOF )
            // InternalPolicyDsl.g:863:2: iv_ruleFQN= ruleFQN EOF
            {
             newCompositeNode(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;

             current =iv_ruleFQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalPolicyDsl.g:869:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:875:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalPolicyDsl.g:876:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalPolicyDsl.g:876:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalPolicyDsl.g:877:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_21); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0());
            		
            // InternalPolicyDsl.g:884:3: (kw= '.' this_ID_2= RULE_ID )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==30) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPolicyDsl.g:885:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,30,FOLLOW_5); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_21); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "ruleActionKind"
    // InternalPolicyDsl.g:902:1: ruleActionKind returns [Enumerator current=null] : ( (enumLiteral_0= 'Read' ) | (enumLiteral_1= 'Write' ) | (enumLiteral_2= 'Execute' ) ) ;
    public final Enumerator ruleActionKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:908:2: ( ( (enumLiteral_0= 'Read' ) | (enumLiteral_1= 'Write' ) | (enumLiteral_2= 'Execute' ) ) )
            // InternalPolicyDsl.g:909:2: ( (enumLiteral_0= 'Read' ) | (enumLiteral_1= 'Write' ) | (enumLiteral_2= 'Execute' ) )
            {
            // InternalPolicyDsl.g:909:2: ( (enumLiteral_0= 'Read' ) | (enumLiteral_1= 'Write' ) | (enumLiteral_2= 'Execute' ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt8=1;
                }
                break;
            case 32:
                {
                alt8=2;
                }
                break;
            case 33:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalPolicyDsl.g:910:3: (enumLiteral_0= 'Read' )
                    {
                    // InternalPolicyDsl.g:910:3: (enumLiteral_0= 'Read' )
                    // InternalPolicyDsl.g:911:4: enumLiteral_0= 'Read'
                    {
                    enumLiteral_0=(Token)match(input,31,FOLLOW_2); 

                    				current = grammarAccess.getActionKindAccess().getReadEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getActionKindAccess().getReadEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPolicyDsl.g:918:3: (enumLiteral_1= 'Write' )
                    {
                    // InternalPolicyDsl.g:918:3: (enumLiteral_1= 'Write' )
                    // InternalPolicyDsl.g:919:4: enumLiteral_1= 'Write'
                    {
                    enumLiteral_1=(Token)match(input,32,FOLLOW_2); 

                    				current = grammarAccess.getActionKindAccess().getWriteEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getActionKindAccess().getWriteEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalPolicyDsl.g:926:3: (enumLiteral_2= 'Execute' )
                    {
                    // InternalPolicyDsl.g:926:3: (enumLiteral_2= 'Execute' )
                    // InternalPolicyDsl.g:927:4: enumLiteral_2= 'Execute'
                    {
                    enumLiteral_2=(Token)match(input,33,FOLLOW_2); 

                    				current = grammarAccess.getActionKindAccess().getExecuteEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getActionKindAccess().getExecuteEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionKind"


    // $ANTLR start "ruleDecisionKind"
    // InternalPolicyDsl.g:937:1: ruleDecisionKind returns [Enumerator current=null] : ( (enumLiteral_0= 'Accept' ) | (enumLiteral_1= 'Deny' ) | (enumLiteral_2= 'Undetermined' ) ) ;
    public final Enumerator ruleDecisionKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalPolicyDsl.g:943:2: ( ( (enumLiteral_0= 'Accept' ) | (enumLiteral_1= 'Deny' ) | (enumLiteral_2= 'Undetermined' ) ) )
            // InternalPolicyDsl.g:944:2: ( (enumLiteral_0= 'Accept' ) | (enumLiteral_1= 'Deny' ) | (enumLiteral_2= 'Undetermined' ) )
            {
            // InternalPolicyDsl.g:944:2: ( (enumLiteral_0= 'Accept' ) | (enumLiteral_1= 'Deny' ) | (enumLiteral_2= 'Undetermined' ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 34:
                {
                alt9=1;
                }
                break;
            case 35:
                {
                alt9=2;
                }
                break;
            case 36:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalPolicyDsl.g:945:3: (enumLiteral_0= 'Accept' )
                    {
                    // InternalPolicyDsl.g:945:3: (enumLiteral_0= 'Accept' )
                    // InternalPolicyDsl.g:946:4: enumLiteral_0= 'Accept'
                    {
                    enumLiteral_0=(Token)match(input,34,FOLLOW_2); 

                    				current = grammarAccess.getDecisionKindAccess().getAcceptEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDecisionKindAccess().getAcceptEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPolicyDsl.g:953:3: (enumLiteral_1= 'Deny' )
                    {
                    // InternalPolicyDsl.g:953:3: (enumLiteral_1= 'Deny' )
                    // InternalPolicyDsl.g:954:4: enumLiteral_1= 'Deny'
                    {
                    enumLiteral_1=(Token)match(input,35,FOLLOW_2); 

                    				current = grammarAccess.getDecisionKindAccess().getDenyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDecisionKindAccess().getDenyEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalPolicyDsl.g:961:3: (enumLiteral_2= 'Undetermined' )
                    {
                    // InternalPolicyDsl.g:961:3: (enumLiteral_2= 'Undetermined' )
                    // InternalPolicyDsl.g:962:4: enumLiteral_2= 'Undetermined'
                    {
                    enumLiteral_2=(Token)match(input,36,FOLLOW_2); 

                    				current = grammarAccess.getDecisionKindAccess().getUndeterminedEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDecisionKindAccess().getUndeterminedEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDecisionKind"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000002802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000001C00000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000380000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000038400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000040000002L});

}