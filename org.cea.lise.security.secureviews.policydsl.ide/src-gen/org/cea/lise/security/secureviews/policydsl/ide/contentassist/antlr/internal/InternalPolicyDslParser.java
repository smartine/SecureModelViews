package org.cea.lise.security.secureviews.policydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.cea.lise.security.secureviews.policydsl.services.PolicyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPolicyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Read'", "'Write'", "'Execute'", "'Accept'", "'Deny'", "'Undetermined'", "'DeclareRole'", "'import'", "'rule'", "'('", "')'", "'->'", "'Subject'", "';'", "'Action'", "'Object'", "','", "'class'", "'WithValue'", "'='", "'<'", "'>'", "'ref'", "'att'", "'op'", "'.'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPolicyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPolicyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPolicyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPolicyDsl.g"; }


    	private PolicyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(PolicyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRulePolicy"
    // InternalPolicyDsl.g:53:1: entryRulePolicy : rulePolicy EOF ;
    public final void entryRulePolicy() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:54:1: ( rulePolicy EOF )
            // InternalPolicyDsl.g:55:1: rulePolicy EOF
            {
             before(grammarAccess.getPolicyRule()); 
            pushFollow(FOLLOW_1);
            rulePolicy();

            state._fsp--;

             after(grammarAccess.getPolicyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePolicy"


    // $ANTLR start "rulePolicy"
    // InternalPolicyDsl.g:62:1: rulePolicy : ( ( rule__Policy__Group__0 ) ) ;
    public final void rulePolicy() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:66:2: ( ( ( rule__Policy__Group__0 ) ) )
            // InternalPolicyDsl.g:67:2: ( ( rule__Policy__Group__0 ) )
            {
            // InternalPolicyDsl.g:67:2: ( ( rule__Policy__Group__0 ) )
            // InternalPolicyDsl.g:68:3: ( rule__Policy__Group__0 )
            {
             before(grammarAccess.getPolicyAccess().getGroup()); 
            // InternalPolicyDsl.g:69:3: ( rule__Policy__Group__0 )
            // InternalPolicyDsl.g:69:4: rule__Policy__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Policy__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPolicyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePolicy"


    // $ANTLR start "entryRuleRoleDeclaration"
    // InternalPolicyDsl.g:78:1: entryRuleRoleDeclaration : ruleRoleDeclaration EOF ;
    public final void entryRuleRoleDeclaration() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:79:1: ( ruleRoleDeclaration EOF )
            // InternalPolicyDsl.g:80:1: ruleRoleDeclaration EOF
            {
             before(grammarAccess.getRoleDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleRoleDeclaration();

            state._fsp--;

             after(grammarAccess.getRoleDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoleDeclaration"


    // $ANTLR start "ruleRoleDeclaration"
    // InternalPolicyDsl.g:87:1: ruleRoleDeclaration : ( ( rule__RoleDeclaration__Group__0 ) ) ;
    public final void ruleRoleDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:91:2: ( ( ( rule__RoleDeclaration__Group__0 ) ) )
            // InternalPolicyDsl.g:92:2: ( ( rule__RoleDeclaration__Group__0 ) )
            {
            // InternalPolicyDsl.g:92:2: ( ( rule__RoleDeclaration__Group__0 ) )
            // InternalPolicyDsl.g:93:3: ( rule__RoleDeclaration__Group__0 )
            {
             before(grammarAccess.getRoleDeclarationAccess().getGroup()); 
            // InternalPolicyDsl.g:94:3: ( rule__RoleDeclaration__Group__0 )
            // InternalPolicyDsl.g:94:4: rule__RoleDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RoleDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRoleDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoleDeclaration"


    // $ANTLR start "entryRuleImport"
    // InternalPolicyDsl.g:103:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:104:1: ( ruleImport EOF )
            // InternalPolicyDsl.g:105:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalPolicyDsl.g:112:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:116:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalPolicyDsl.g:117:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalPolicyDsl.g:117:2: ( ( rule__Import__Group__0 ) )
            // InternalPolicyDsl.g:118:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalPolicyDsl.g:119:3: ( rule__Import__Group__0 )
            // InternalPolicyDsl.g:119:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleRule"
    // InternalPolicyDsl.g:128:1: entryRuleRule : ruleRule EOF ;
    public final void entryRuleRule() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:129:1: ( ruleRule EOF )
            // InternalPolicyDsl.g:130:1: ruleRule EOF
            {
             before(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleRule();

            state._fsp--;

             after(grammarAccess.getRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // InternalPolicyDsl.g:137:1: ruleRule : ( ( rule__Rule__Group__0 ) ) ;
    public final void ruleRule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:141:2: ( ( ( rule__Rule__Group__0 ) ) )
            // InternalPolicyDsl.g:142:2: ( ( rule__Rule__Group__0 ) )
            {
            // InternalPolicyDsl.g:142:2: ( ( rule__Rule__Group__0 ) )
            // InternalPolicyDsl.g:143:3: ( rule__Rule__Group__0 )
            {
             before(grammarAccess.getRuleAccess().getGroup()); 
            // InternalPolicyDsl.g:144:3: ( rule__Rule__Group__0 )
            // InternalPolicyDsl.g:144:4: rule__Rule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRuleLHS"
    // InternalPolicyDsl.g:153:1: entryRuleLHS : ruleLHS EOF ;
    public final void entryRuleLHS() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:154:1: ( ruleLHS EOF )
            // InternalPolicyDsl.g:155:1: ruleLHS EOF
            {
             before(grammarAccess.getLHSRule()); 
            pushFollow(FOLLOW_1);
            ruleLHS();

            state._fsp--;

             after(grammarAccess.getLHSRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLHS"


    // $ANTLR start "ruleLHS"
    // InternalPolicyDsl.g:162:1: ruleLHS : ( ( rule__LHS__Group__0 ) ) ;
    public final void ruleLHS() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:166:2: ( ( ( rule__LHS__Group__0 ) ) )
            // InternalPolicyDsl.g:167:2: ( ( rule__LHS__Group__0 ) )
            {
            // InternalPolicyDsl.g:167:2: ( ( rule__LHS__Group__0 ) )
            // InternalPolicyDsl.g:168:3: ( rule__LHS__Group__0 )
            {
             before(grammarAccess.getLHSAccess().getGroup()); 
            // InternalPolicyDsl.g:169:3: ( rule__LHS__Group__0 )
            // InternalPolicyDsl.g:169:4: rule__LHS__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LHS__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLHSAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLHS"


    // $ANTLR start "entryRuleRHS"
    // InternalPolicyDsl.g:178:1: entryRuleRHS : ruleRHS EOF ;
    public final void entryRuleRHS() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:179:1: ( ruleRHS EOF )
            // InternalPolicyDsl.g:180:1: ruleRHS EOF
            {
             before(grammarAccess.getRHSRule()); 
            pushFollow(FOLLOW_1);
            ruleRHS();

            state._fsp--;

             after(grammarAccess.getRHSRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRHS"


    // $ANTLR start "ruleRHS"
    // InternalPolicyDsl.g:187:1: ruleRHS : ( ( rule__RHS__DecisionsAssignment ) ) ;
    public final void ruleRHS() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:191:2: ( ( ( rule__RHS__DecisionsAssignment ) ) )
            // InternalPolicyDsl.g:192:2: ( ( rule__RHS__DecisionsAssignment ) )
            {
            // InternalPolicyDsl.g:192:2: ( ( rule__RHS__DecisionsAssignment ) )
            // InternalPolicyDsl.g:193:3: ( rule__RHS__DecisionsAssignment )
            {
             before(grammarAccess.getRHSAccess().getDecisionsAssignment()); 
            // InternalPolicyDsl.g:194:3: ( rule__RHS__DecisionsAssignment )
            // InternalPolicyDsl.g:194:4: rule__RHS__DecisionsAssignment
            {
            pushFollow(FOLLOW_2);
            rule__RHS__DecisionsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRHSAccess().getDecisionsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRHS"


    // $ANTLR start "entryRuleSubject"
    // InternalPolicyDsl.g:203:1: entryRuleSubject : ruleSubject EOF ;
    public final void entryRuleSubject() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:204:1: ( ruleSubject EOF )
            // InternalPolicyDsl.g:205:1: ruleSubject EOF
            {
             before(grammarAccess.getSubjectRule()); 
            pushFollow(FOLLOW_1);
            ruleSubject();

            state._fsp--;

             after(grammarAccess.getSubjectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSubject"


    // $ANTLR start "ruleSubject"
    // InternalPolicyDsl.g:212:1: ruleSubject : ( ( rule__Subject__Group__0 ) ) ;
    public final void ruleSubject() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:216:2: ( ( ( rule__Subject__Group__0 ) ) )
            // InternalPolicyDsl.g:217:2: ( ( rule__Subject__Group__0 ) )
            {
            // InternalPolicyDsl.g:217:2: ( ( rule__Subject__Group__0 ) )
            // InternalPolicyDsl.g:218:3: ( rule__Subject__Group__0 )
            {
             before(grammarAccess.getSubjectAccess().getGroup()); 
            // InternalPolicyDsl.g:219:3: ( rule__Subject__Group__0 )
            // InternalPolicyDsl.g:219:4: rule__Subject__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Subject__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSubjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSubject"


    // $ANTLR start "entryRuleAction"
    // InternalPolicyDsl.g:228:1: entryRuleAction : ruleAction EOF ;
    public final void entryRuleAction() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:229:1: ( ruleAction EOF )
            // InternalPolicyDsl.g:230:1: ruleAction EOF
            {
             before(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getActionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalPolicyDsl.g:237:1: ruleAction : ( ( rule__Action__Group__0 ) ) ;
    public final void ruleAction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:241:2: ( ( ( rule__Action__Group__0 ) ) )
            // InternalPolicyDsl.g:242:2: ( ( rule__Action__Group__0 ) )
            {
            // InternalPolicyDsl.g:242:2: ( ( rule__Action__Group__0 ) )
            // InternalPolicyDsl.g:243:3: ( rule__Action__Group__0 )
            {
             before(grammarAccess.getActionAccess().getGroup()); 
            // InternalPolicyDsl.g:244:3: ( rule__Action__Group__0 )
            // InternalPolicyDsl.g:244:4: rule__Action__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleObjectCondition"
    // InternalPolicyDsl.g:253:1: entryRuleObjectCondition : ruleObjectCondition EOF ;
    public final void entryRuleObjectCondition() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:254:1: ( ruleObjectCondition EOF )
            // InternalPolicyDsl.g:255:1: ruleObjectCondition EOF
            {
             before(grammarAccess.getObjectConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleObjectCondition();

            state._fsp--;

             after(grammarAccess.getObjectConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjectCondition"


    // $ANTLR start "ruleObjectCondition"
    // InternalPolicyDsl.g:262:1: ruleObjectCondition : ( ( rule__ObjectCondition__Alternatives ) ) ;
    public final void ruleObjectCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:266:2: ( ( ( rule__ObjectCondition__Alternatives ) ) )
            // InternalPolicyDsl.g:267:2: ( ( rule__ObjectCondition__Alternatives ) )
            {
            // InternalPolicyDsl.g:267:2: ( ( rule__ObjectCondition__Alternatives ) )
            // InternalPolicyDsl.g:268:3: ( rule__ObjectCondition__Alternatives )
            {
             before(grammarAccess.getObjectConditionAccess().getAlternatives()); 
            // InternalPolicyDsl.g:269:3: ( rule__ObjectCondition__Alternatives )
            // InternalPolicyDsl.g:269:4: rule__ObjectCondition__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ObjectCondition__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getObjectConditionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectCondition"


    // $ANTLR start "entryRuleClassCondition"
    // InternalPolicyDsl.g:278:1: entryRuleClassCondition : ruleClassCondition EOF ;
    public final void entryRuleClassCondition() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:279:1: ( ruleClassCondition EOF )
            // InternalPolicyDsl.g:280:1: ruleClassCondition EOF
            {
             before(grammarAccess.getClassConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleClassCondition();

            state._fsp--;

             after(grammarAccess.getClassConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassCondition"


    // $ANTLR start "ruleClassCondition"
    // InternalPolicyDsl.g:287:1: ruleClassCondition : ( ( rule__ClassCondition__Group__0 ) ) ;
    public final void ruleClassCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:291:2: ( ( ( rule__ClassCondition__Group__0 ) ) )
            // InternalPolicyDsl.g:292:2: ( ( rule__ClassCondition__Group__0 ) )
            {
            // InternalPolicyDsl.g:292:2: ( ( rule__ClassCondition__Group__0 ) )
            // InternalPolicyDsl.g:293:3: ( rule__ClassCondition__Group__0 )
            {
             before(grammarAccess.getClassConditionAccess().getGroup()); 
            // InternalPolicyDsl.g:294:3: ( rule__ClassCondition__Group__0 )
            // InternalPolicyDsl.g:294:4: rule__ClassCondition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassCondition"


    // $ANTLR start "entryRuleReferenceCondition"
    // InternalPolicyDsl.g:303:1: entryRuleReferenceCondition : ruleReferenceCondition EOF ;
    public final void entryRuleReferenceCondition() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:304:1: ( ruleReferenceCondition EOF )
            // InternalPolicyDsl.g:305:1: ruleReferenceCondition EOF
            {
             before(grammarAccess.getReferenceConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleReferenceCondition();

            state._fsp--;

             after(grammarAccess.getReferenceConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReferenceCondition"


    // $ANTLR start "ruleReferenceCondition"
    // InternalPolicyDsl.g:312:1: ruleReferenceCondition : ( ( rule__ReferenceCondition__Group__0 ) ) ;
    public final void ruleReferenceCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:316:2: ( ( ( rule__ReferenceCondition__Group__0 ) ) )
            // InternalPolicyDsl.g:317:2: ( ( rule__ReferenceCondition__Group__0 ) )
            {
            // InternalPolicyDsl.g:317:2: ( ( rule__ReferenceCondition__Group__0 ) )
            // InternalPolicyDsl.g:318:3: ( rule__ReferenceCondition__Group__0 )
            {
             before(grammarAccess.getReferenceConditionAccess().getGroup()); 
            // InternalPolicyDsl.g:319:3: ( rule__ReferenceCondition__Group__0 )
            // InternalPolicyDsl.g:319:4: rule__ReferenceCondition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceCondition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReferenceCondition"


    // $ANTLR start "entryRuleFeatureCondition"
    // InternalPolicyDsl.g:328:1: entryRuleFeatureCondition : ruleFeatureCondition EOF ;
    public final void entryRuleFeatureCondition() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:329:1: ( ruleFeatureCondition EOF )
            // InternalPolicyDsl.g:330:1: ruleFeatureCondition EOF
            {
             before(grammarAccess.getFeatureConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleFeatureCondition();

            state._fsp--;

             after(grammarAccess.getFeatureConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureCondition"


    // $ANTLR start "ruleFeatureCondition"
    // InternalPolicyDsl.g:337:1: ruleFeatureCondition : ( ( rule__FeatureCondition__Group__0 ) ) ;
    public final void ruleFeatureCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:341:2: ( ( ( rule__FeatureCondition__Group__0 ) ) )
            // InternalPolicyDsl.g:342:2: ( ( rule__FeatureCondition__Group__0 ) )
            {
            // InternalPolicyDsl.g:342:2: ( ( rule__FeatureCondition__Group__0 ) )
            // InternalPolicyDsl.g:343:3: ( rule__FeatureCondition__Group__0 )
            {
             before(grammarAccess.getFeatureConditionAccess().getGroup()); 
            // InternalPolicyDsl.g:344:3: ( rule__FeatureCondition__Group__0 )
            // InternalPolicyDsl.g:344:4: rule__FeatureCondition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FeatureCondition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureCondition"


    // $ANTLR start "entryRuleOperationCondition"
    // InternalPolicyDsl.g:353:1: entryRuleOperationCondition : ruleOperationCondition EOF ;
    public final void entryRuleOperationCondition() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:354:1: ( ruleOperationCondition EOF )
            // InternalPolicyDsl.g:355:1: ruleOperationCondition EOF
            {
             before(grammarAccess.getOperationConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleOperationCondition();

            state._fsp--;

             after(grammarAccess.getOperationConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOperationCondition"


    // $ANTLR start "ruleOperationCondition"
    // InternalPolicyDsl.g:362:1: ruleOperationCondition : ( ( rule__OperationCondition__Group__0 ) ) ;
    public final void ruleOperationCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:366:2: ( ( ( rule__OperationCondition__Group__0 ) ) )
            // InternalPolicyDsl.g:367:2: ( ( rule__OperationCondition__Group__0 ) )
            {
            // InternalPolicyDsl.g:367:2: ( ( rule__OperationCondition__Group__0 ) )
            // InternalPolicyDsl.g:368:3: ( rule__OperationCondition__Group__0 )
            {
             before(grammarAccess.getOperationConditionAccess().getGroup()); 
            // InternalPolicyDsl.g:369:3: ( rule__OperationCondition__Group__0 )
            // InternalPolicyDsl.g:369:4: rule__OperationCondition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OperationCondition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOperationConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOperationCondition"


    // $ANTLR start "entryRuleAccessDecision"
    // InternalPolicyDsl.g:378:1: entryRuleAccessDecision : ruleAccessDecision EOF ;
    public final void entryRuleAccessDecision() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:379:1: ( ruleAccessDecision EOF )
            // InternalPolicyDsl.g:380:1: ruleAccessDecision EOF
            {
             before(grammarAccess.getAccessDecisionRule()); 
            pushFollow(FOLLOW_1);
            ruleAccessDecision();

            state._fsp--;

             after(grammarAccess.getAccessDecisionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAccessDecision"


    // $ANTLR start "ruleAccessDecision"
    // InternalPolicyDsl.g:387:1: ruleAccessDecision : ( ( rule__AccessDecision__DecisionAssignment ) ) ;
    public final void ruleAccessDecision() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:391:2: ( ( ( rule__AccessDecision__DecisionAssignment ) ) )
            // InternalPolicyDsl.g:392:2: ( ( rule__AccessDecision__DecisionAssignment ) )
            {
            // InternalPolicyDsl.g:392:2: ( ( rule__AccessDecision__DecisionAssignment ) )
            // InternalPolicyDsl.g:393:3: ( rule__AccessDecision__DecisionAssignment )
            {
             before(grammarAccess.getAccessDecisionAccess().getDecisionAssignment()); 
            // InternalPolicyDsl.g:394:3: ( rule__AccessDecision__DecisionAssignment )
            // InternalPolicyDsl.g:394:4: rule__AccessDecision__DecisionAssignment
            {
            pushFollow(FOLLOW_2);
            rule__AccessDecision__DecisionAssignment();

            state._fsp--;


            }

             after(grammarAccess.getAccessDecisionAccess().getDecisionAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAccessDecision"


    // $ANTLR start "entryRuleFQN"
    // InternalPolicyDsl.g:403:1: entryRuleFQN : ruleFQN EOF ;
    public final void entryRuleFQN() throws RecognitionException {
        try {
            // InternalPolicyDsl.g:404:1: ( ruleFQN EOF )
            // InternalPolicyDsl.g:405:1: ruleFQN EOF
            {
             before(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getFQNRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalPolicyDsl.g:412:1: ruleFQN : ( ( rule__FQN__Group__0 ) ) ;
    public final void ruleFQN() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:416:2: ( ( ( rule__FQN__Group__0 ) ) )
            // InternalPolicyDsl.g:417:2: ( ( rule__FQN__Group__0 ) )
            {
            // InternalPolicyDsl.g:417:2: ( ( rule__FQN__Group__0 ) )
            // InternalPolicyDsl.g:418:3: ( rule__FQN__Group__0 )
            {
             before(grammarAccess.getFQNAccess().getGroup()); 
            // InternalPolicyDsl.g:419:3: ( rule__FQN__Group__0 )
            // InternalPolicyDsl.g:419:4: rule__FQN__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFQNAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "ruleActionKind"
    // InternalPolicyDsl.g:428:1: ruleActionKind : ( ( rule__ActionKind__Alternatives ) ) ;
    public final void ruleActionKind() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:432:1: ( ( ( rule__ActionKind__Alternatives ) ) )
            // InternalPolicyDsl.g:433:2: ( ( rule__ActionKind__Alternatives ) )
            {
            // InternalPolicyDsl.g:433:2: ( ( rule__ActionKind__Alternatives ) )
            // InternalPolicyDsl.g:434:3: ( rule__ActionKind__Alternatives )
            {
             before(grammarAccess.getActionKindAccess().getAlternatives()); 
            // InternalPolicyDsl.g:435:3: ( rule__ActionKind__Alternatives )
            // InternalPolicyDsl.g:435:4: rule__ActionKind__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ActionKind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getActionKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleActionKind"


    // $ANTLR start "ruleDecisionKind"
    // InternalPolicyDsl.g:444:1: ruleDecisionKind : ( ( rule__DecisionKind__Alternatives ) ) ;
    public final void ruleDecisionKind() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:448:1: ( ( ( rule__DecisionKind__Alternatives ) ) )
            // InternalPolicyDsl.g:449:2: ( ( rule__DecisionKind__Alternatives ) )
            {
            // InternalPolicyDsl.g:449:2: ( ( rule__DecisionKind__Alternatives ) )
            // InternalPolicyDsl.g:450:3: ( rule__DecisionKind__Alternatives )
            {
             before(grammarAccess.getDecisionKindAccess().getAlternatives()); 
            // InternalPolicyDsl.g:451:3: ( rule__DecisionKind__Alternatives )
            // InternalPolicyDsl.g:451:4: rule__DecisionKind__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DecisionKind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDecisionKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDecisionKind"


    // $ANTLR start "rule__ObjectCondition__Alternatives"
    // InternalPolicyDsl.g:459:1: rule__ObjectCondition__Alternatives : ( ( ruleClassCondition ) | ( ruleFeatureCondition ) | ( ruleReferenceCondition ) | ( ruleOperationCondition ) );
    public final void rule__ObjectCondition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:463:1: ( ( ruleClassCondition ) | ( ruleFeatureCondition ) | ( ruleReferenceCondition ) | ( ruleOperationCondition ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 28:
                {
                alt1=1;
                }
                break;
            case 34:
                {
                alt1=2;
                }
                break;
            case 33:
                {
                alt1=3;
                }
                break;
            case 35:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalPolicyDsl.g:464:2: ( ruleClassCondition )
                    {
                    // InternalPolicyDsl.g:464:2: ( ruleClassCondition )
                    // InternalPolicyDsl.g:465:3: ruleClassCondition
                    {
                     before(grammarAccess.getObjectConditionAccess().getClassConditionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleClassCondition();

                    state._fsp--;

                     after(grammarAccess.getObjectConditionAccess().getClassConditionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPolicyDsl.g:470:2: ( ruleFeatureCondition )
                    {
                    // InternalPolicyDsl.g:470:2: ( ruleFeatureCondition )
                    // InternalPolicyDsl.g:471:3: ruleFeatureCondition
                    {
                     before(grammarAccess.getObjectConditionAccess().getFeatureConditionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleFeatureCondition();

                    state._fsp--;

                     after(grammarAccess.getObjectConditionAccess().getFeatureConditionParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPolicyDsl.g:476:2: ( ruleReferenceCondition )
                    {
                    // InternalPolicyDsl.g:476:2: ( ruleReferenceCondition )
                    // InternalPolicyDsl.g:477:3: ruleReferenceCondition
                    {
                     before(grammarAccess.getObjectConditionAccess().getReferenceConditionParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleReferenceCondition();

                    state._fsp--;

                     after(grammarAccess.getObjectConditionAccess().getReferenceConditionParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalPolicyDsl.g:482:2: ( ruleOperationCondition )
                    {
                    // InternalPolicyDsl.g:482:2: ( ruleOperationCondition )
                    // InternalPolicyDsl.g:483:3: ruleOperationCondition
                    {
                     before(grammarAccess.getObjectConditionAccess().getOperationConditionParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleOperationCondition();

                    state._fsp--;

                     after(grammarAccess.getObjectConditionAccess().getOperationConditionParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectCondition__Alternatives"


    // $ANTLR start "rule__ActionKind__Alternatives"
    // InternalPolicyDsl.g:492:1: rule__ActionKind__Alternatives : ( ( ( 'Read' ) ) | ( ( 'Write' ) ) | ( ( 'Execute' ) ) );
    public final void rule__ActionKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:496:1: ( ( ( 'Read' ) ) | ( ( 'Write' ) ) | ( ( 'Execute' ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalPolicyDsl.g:497:2: ( ( 'Read' ) )
                    {
                    // InternalPolicyDsl.g:497:2: ( ( 'Read' ) )
                    // InternalPolicyDsl.g:498:3: ( 'Read' )
                    {
                     before(grammarAccess.getActionKindAccess().getReadEnumLiteralDeclaration_0()); 
                    // InternalPolicyDsl.g:499:3: ( 'Read' )
                    // InternalPolicyDsl.g:499:4: 'Read'
                    {
                    match(input,11,FOLLOW_2); 

                    }

                     after(grammarAccess.getActionKindAccess().getReadEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPolicyDsl.g:503:2: ( ( 'Write' ) )
                    {
                    // InternalPolicyDsl.g:503:2: ( ( 'Write' ) )
                    // InternalPolicyDsl.g:504:3: ( 'Write' )
                    {
                     before(grammarAccess.getActionKindAccess().getWriteEnumLiteralDeclaration_1()); 
                    // InternalPolicyDsl.g:505:3: ( 'Write' )
                    // InternalPolicyDsl.g:505:4: 'Write'
                    {
                    match(input,12,FOLLOW_2); 

                    }

                     after(grammarAccess.getActionKindAccess().getWriteEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPolicyDsl.g:509:2: ( ( 'Execute' ) )
                    {
                    // InternalPolicyDsl.g:509:2: ( ( 'Execute' ) )
                    // InternalPolicyDsl.g:510:3: ( 'Execute' )
                    {
                     before(grammarAccess.getActionKindAccess().getExecuteEnumLiteralDeclaration_2()); 
                    // InternalPolicyDsl.g:511:3: ( 'Execute' )
                    // InternalPolicyDsl.g:511:4: 'Execute'
                    {
                    match(input,13,FOLLOW_2); 

                    }

                     after(grammarAccess.getActionKindAccess().getExecuteEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ActionKind__Alternatives"


    // $ANTLR start "rule__DecisionKind__Alternatives"
    // InternalPolicyDsl.g:519:1: rule__DecisionKind__Alternatives : ( ( ( 'Accept' ) ) | ( ( 'Deny' ) ) | ( ( 'Undetermined' ) ) );
    public final void rule__DecisionKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:523:1: ( ( ( 'Accept' ) ) | ( ( 'Deny' ) ) | ( ( 'Undetermined' ) ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt3=1;
                }
                break;
            case 15:
                {
                alt3=2;
                }
                break;
            case 16:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalPolicyDsl.g:524:2: ( ( 'Accept' ) )
                    {
                    // InternalPolicyDsl.g:524:2: ( ( 'Accept' ) )
                    // InternalPolicyDsl.g:525:3: ( 'Accept' )
                    {
                     before(grammarAccess.getDecisionKindAccess().getAcceptEnumLiteralDeclaration_0()); 
                    // InternalPolicyDsl.g:526:3: ( 'Accept' )
                    // InternalPolicyDsl.g:526:4: 'Accept'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getDecisionKindAccess().getAcceptEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPolicyDsl.g:530:2: ( ( 'Deny' ) )
                    {
                    // InternalPolicyDsl.g:530:2: ( ( 'Deny' ) )
                    // InternalPolicyDsl.g:531:3: ( 'Deny' )
                    {
                     before(grammarAccess.getDecisionKindAccess().getDenyEnumLiteralDeclaration_1()); 
                    // InternalPolicyDsl.g:532:3: ( 'Deny' )
                    // InternalPolicyDsl.g:532:4: 'Deny'
                    {
                    match(input,15,FOLLOW_2); 

                    }

                     after(grammarAccess.getDecisionKindAccess().getDenyEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPolicyDsl.g:536:2: ( ( 'Undetermined' ) )
                    {
                    // InternalPolicyDsl.g:536:2: ( ( 'Undetermined' ) )
                    // InternalPolicyDsl.g:537:3: ( 'Undetermined' )
                    {
                     before(grammarAccess.getDecisionKindAccess().getUndeterminedEnumLiteralDeclaration_2()); 
                    // InternalPolicyDsl.g:538:3: ( 'Undetermined' )
                    // InternalPolicyDsl.g:538:4: 'Undetermined'
                    {
                    match(input,16,FOLLOW_2); 

                    }

                     after(grammarAccess.getDecisionKindAccess().getUndeterminedEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DecisionKind__Alternatives"


    // $ANTLR start "rule__Policy__Group__0"
    // InternalPolicyDsl.g:546:1: rule__Policy__Group__0 : rule__Policy__Group__0__Impl rule__Policy__Group__1 ;
    public final void rule__Policy__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:550:1: ( rule__Policy__Group__0__Impl rule__Policy__Group__1 )
            // InternalPolicyDsl.g:551:2: rule__Policy__Group__0__Impl rule__Policy__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Policy__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Policy__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__Group__0"


    // $ANTLR start "rule__Policy__Group__0__Impl"
    // InternalPolicyDsl.g:558:1: rule__Policy__Group__0__Impl : ( ( rule__Policy__ImportsAssignment_0 ) ) ;
    public final void rule__Policy__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:562:1: ( ( ( rule__Policy__ImportsAssignment_0 ) ) )
            // InternalPolicyDsl.g:563:1: ( ( rule__Policy__ImportsAssignment_0 ) )
            {
            // InternalPolicyDsl.g:563:1: ( ( rule__Policy__ImportsAssignment_0 ) )
            // InternalPolicyDsl.g:564:2: ( rule__Policy__ImportsAssignment_0 )
            {
             before(grammarAccess.getPolicyAccess().getImportsAssignment_0()); 
            // InternalPolicyDsl.g:565:2: ( rule__Policy__ImportsAssignment_0 )
            // InternalPolicyDsl.g:565:3: rule__Policy__ImportsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Policy__ImportsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPolicyAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__Group__0__Impl"


    // $ANTLR start "rule__Policy__Group__1"
    // InternalPolicyDsl.g:573:1: rule__Policy__Group__1 : rule__Policy__Group__1__Impl rule__Policy__Group__2 ;
    public final void rule__Policy__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:577:1: ( rule__Policy__Group__1__Impl rule__Policy__Group__2 )
            // InternalPolicyDsl.g:578:2: rule__Policy__Group__1__Impl rule__Policy__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Policy__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Policy__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__Group__1"


    // $ANTLR start "rule__Policy__Group__1__Impl"
    // InternalPolicyDsl.g:585:1: rule__Policy__Group__1__Impl : ( ( rule__Policy__RoledeclarationAssignment_1 )* ) ;
    public final void rule__Policy__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:589:1: ( ( ( rule__Policy__RoledeclarationAssignment_1 )* ) )
            // InternalPolicyDsl.g:590:1: ( ( rule__Policy__RoledeclarationAssignment_1 )* )
            {
            // InternalPolicyDsl.g:590:1: ( ( rule__Policy__RoledeclarationAssignment_1 )* )
            // InternalPolicyDsl.g:591:2: ( rule__Policy__RoledeclarationAssignment_1 )*
            {
             before(grammarAccess.getPolicyAccess().getRoledeclarationAssignment_1()); 
            // InternalPolicyDsl.g:592:2: ( rule__Policy__RoledeclarationAssignment_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPolicyDsl.g:592:3: rule__Policy__RoledeclarationAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Policy__RoledeclarationAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getPolicyAccess().getRoledeclarationAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__Group__1__Impl"


    // $ANTLR start "rule__Policy__Group__2"
    // InternalPolicyDsl.g:600:1: rule__Policy__Group__2 : rule__Policy__Group__2__Impl ;
    public final void rule__Policy__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:604:1: ( rule__Policy__Group__2__Impl )
            // InternalPolicyDsl.g:605:2: rule__Policy__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Policy__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__Group__2"


    // $ANTLR start "rule__Policy__Group__2__Impl"
    // InternalPolicyDsl.g:611:1: rule__Policy__Group__2__Impl : ( ( rule__Policy__RulesAssignment_2 )* ) ;
    public final void rule__Policy__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:615:1: ( ( ( rule__Policy__RulesAssignment_2 )* ) )
            // InternalPolicyDsl.g:616:1: ( ( rule__Policy__RulesAssignment_2 )* )
            {
            // InternalPolicyDsl.g:616:1: ( ( rule__Policy__RulesAssignment_2 )* )
            // InternalPolicyDsl.g:617:2: ( rule__Policy__RulesAssignment_2 )*
            {
             before(grammarAccess.getPolicyAccess().getRulesAssignment_2()); 
            // InternalPolicyDsl.g:618:2: ( rule__Policy__RulesAssignment_2 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==19) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalPolicyDsl.g:618:3: rule__Policy__RulesAssignment_2
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Policy__RulesAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getPolicyAccess().getRulesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__Group__2__Impl"


    // $ANTLR start "rule__RoleDeclaration__Group__0"
    // InternalPolicyDsl.g:627:1: rule__RoleDeclaration__Group__0 : rule__RoleDeclaration__Group__0__Impl rule__RoleDeclaration__Group__1 ;
    public final void rule__RoleDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:631:1: ( rule__RoleDeclaration__Group__0__Impl rule__RoleDeclaration__Group__1 )
            // InternalPolicyDsl.g:632:2: rule__RoleDeclaration__Group__0__Impl rule__RoleDeclaration__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__RoleDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleDeclaration__Group__0"


    // $ANTLR start "rule__RoleDeclaration__Group__0__Impl"
    // InternalPolicyDsl.g:639:1: rule__RoleDeclaration__Group__0__Impl : ( 'DeclareRole' ) ;
    public final void rule__RoleDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:643:1: ( ( 'DeclareRole' ) )
            // InternalPolicyDsl.g:644:1: ( 'DeclareRole' )
            {
            // InternalPolicyDsl.g:644:1: ( 'DeclareRole' )
            // InternalPolicyDsl.g:645:2: 'DeclareRole'
            {
             before(grammarAccess.getRoleDeclarationAccess().getDeclareRoleKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getRoleDeclarationAccess().getDeclareRoleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleDeclaration__Group__0__Impl"


    // $ANTLR start "rule__RoleDeclaration__Group__1"
    // InternalPolicyDsl.g:654:1: rule__RoleDeclaration__Group__1 : rule__RoleDeclaration__Group__1__Impl ;
    public final void rule__RoleDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:658:1: ( rule__RoleDeclaration__Group__1__Impl )
            // InternalPolicyDsl.g:659:2: rule__RoleDeclaration__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RoleDeclaration__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleDeclaration__Group__1"


    // $ANTLR start "rule__RoleDeclaration__Group__1__Impl"
    // InternalPolicyDsl.g:665:1: rule__RoleDeclaration__Group__1__Impl : ( ( rule__RoleDeclaration__NameAssignment_1 ) ) ;
    public final void rule__RoleDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:669:1: ( ( ( rule__RoleDeclaration__NameAssignment_1 ) ) )
            // InternalPolicyDsl.g:670:1: ( ( rule__RoleDeclaration__NameAssignment_1 ) )
            {
            // InternalPolicyDsl.g:670:1: ( ( rule__RoleDeclaration__NameAssignment_1 ) )
            // InternalPolicyDsl.g:671:2: ( rule__RoleDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getRoleDeclarationAccess().getNameAssignment_1()); 
            // InternalPolicyDsl.g:672:2: ( rule__RoleDeclaration__NameAssignment_1 )
            // InternalPolicyDsl.g:672:3: rule__RoleDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__RoleDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRoleDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleDeclaration__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalPolicyDsl.g:681:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:685:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalPolicyDsl.g:686:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalPolicyDsl.g:693:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:697:1: ( ( 'import' ) )
            // InternalPolicyDsl.g:698:1: ( 'import' )
            {
            // InternalPolicyDsl.g:698:1: ( 'import' )
            // InternalPolicyDsl.g:699:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalPolicyDsl.g:708:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:712:1: ( rule__Import__Group__1__Impl )
            // InternalPolicyDsl.g:713:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalPolicyDsl.g:719:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:723:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalPolicyDsl.g:724:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalPolicyDsl.g:724:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalPolicyDsl.g:725:2: ( rule__Import__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            // InternalPolicyDsl.g:726:2: ( rule__Import__ImportURIAssignment_1 )
            // InternalPolicyDsl.g:726:3: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Rule__Group__0"
    // InternalPolicyDsl.g:735:1: rule__Rule__Group__0 : rule__Rule__Group__0__Impl rule__Rule__Group__1 ;
    public final void rule__Rule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:739:1: ( rule__Rule__Group__0__Impl rule__Rule__Group__1 )
            // InternalPolicyDsl.g:740:2: rule__Rule__Group__0__Impl rule__Rule__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Rule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__0"


    // $ANTLR start "rule__Rule__Group__0__Impl"
    // InternalPolicyDsl.g:747:1: rule__Rule__Group__0__Impl : ( 'rule' ) ;
    public final void rule__Rule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:751:1: ( ( 'rule' ) )
            // InternalPolicyDsl.g:752:1: ( 'rule' )
            {
            // InternalPolicyDsl.g:752:1: ( 'rule' )
            // InternalPolicyDsl.g:753:2: 'rule'
            {
             before(grammarAccess.getRuleAccess().getRuleKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getRuleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__0__Impl"


    // $ANTLR start "rule__Rule__Group__1"
    // InternalPolicyDsl.g:762:1: rule__Rule__Group__1 : rule__Rule__Group__1__Impl rule__Rule__Group__2 ;
    public final void rule__Rule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:766:1: ( rule__Rule__Group__1__Impl rule__Rule__Group__2 )
            // InternalPolicyDsl.g:767:2: rule__Rule__Group__1__Impl rule__Rule__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Rule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__1"


    // $ANTLR start "rule__Rule__Group__1__Impl"
    // InternalPolicyDsl.g:774:1: rule__Rule__Group__1__Impl : ( ( rule__Rule__IdAssignment_1 ) ) ;
    public final void rule__Rule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:778:1: ( ( ( rule__Rule__IdAssignment_1 ) ) )
            // InternalPolicyDsl.g:779:1: ( ( rule__Rule__IdAssignment_1 ) )
            {
            // InternalPolicyDsl.g:779:1: ( ( rule__Rule__IdAssignment_1 ) )
            // InternalPolicyDsl.g:780:2: ( rule__Rule__IdAssignment_1 )
            {
             before(grammarAccess.getRuleAccess().getIdAssignment_1()); 
            // InternalPolicyDsl.g:781:2: ( rule__Rule__IdAssignment_1 )
            // InternalPolicyDsl.g:781:3: rule__Rule__IdAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Rule__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__1__Impl"


    // $ANTLR start "rule__Rule__Group__2"
    // InternalPolicyDsl.g:789:1: rule__Rule__Group__2 : rule__Rule__Group__2__Impl rule__Rule__Group__3 ;
    public final void rule__Rule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:793:1: ( rule__Rule__Group__2__Impl rule__Rule__Group__3 )
            // InternalPolicyDsl.g:794:2: rule__Rule__Group__2__Impl rule__Rule__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Rule__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__2"


    // $ANTLR start "rule__Rule__Group__2__Impl"
    // InternalPolicyDsl.g:801:1: rule__Rule__Group__2__Impl : ( '(' ) ;
    public final void rule__Rule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:805:1: ( ( '(' ) )
            // InternalPolicyDsl.g:806:1: ( '(' )
            {
            // InternalPolicyDsl.g:806:1: ( '(' )
            // InternalPolicyDsl.g:807:2: '('
            {
             before(grammarAccess.getRuleAccess().getLeftParenthesisKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__2__Impl"


    // $ANTLR start "rule__Rule__Group__3"
    // InternalPolicyDsl.g:816:1: rule__Rule__Group__3 : rule__Rule__Group__3__Impl rule__Rule__Group__4 ;
    public final void rule__Rule__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:820:1: ( rule__Rule__Group__3__Impl rule__Rule__Group__4 )
            // InternalPolicyDsl.g:821:2: rule__Rule__Group__3__Impl rule__Rule__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__Rule__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__3"


    // $ANTLR start "rule__Rule__Group__3__Impl"
    // InternalPolicyDsl.g:828:1: rule__Rule__Group__3__Impl : ( ( rule__Rule__LhsAssignment_3 ) ) ;
    public final void rule__Rule__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:832:1: ( ( ( rule__Rule__LhsAssignment_3 ) ) )
            // InternalPolicyDsl.g:833:1: ( ( rule__Rule__LhsAssignment_3 ) )
            {
            // InternalPolicyDsl.g:833:1: ( ( rule__Rule__LhsAssignment_3 ) )
            // InternalPolicyDsl.g:834:2: ( rule__Rule__LhsAssignment_3 )
            {
             before(grammarAccess.getRuleAccess().getLhsAssignment_3()); 
            // InternalPolicyDsl.g:835:2: ( rule__Rule__LhsAssignment_3 )
            // InternalPolicyDsl.g:835:3: rule__Rule__LhsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Rule__LhsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getLhsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__3__Impl"


    // $ANTLR start "rule__Rule__Group__4"
    // InternalPolicyDsl.g:843:1: rule__Rule__Group__4 : rule__Rule__Group__4__Impl rule__Rule__Group__5 ;
    public final void rule__Rule__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:847:1: ( rule__Rule__Group__4__Impl rule__Rule__Group__5 )
            // InternalPolicyDsl.g:848:2: rule__Rule__Group__4__Impl rule__Rule__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__Rule__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__4"


    // $ANTLR start "rule__Rule__Group__4__Impl"
    // InternalPolicyDsl.g:855:1: rule__Rule__Group__4__Impl : ( ')' ) ;
    public final void rule__Rule__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:859:1: ( ( ')' ) )
            // InternalPolicyDsl.g:860:1: ( ')' )
            {
            // InternalPolicyDsl.g:860:1: ( ')' )
            // InternalPolicyDsl.g:861:2: ')'
            {
             before(grammarAccess.getRuleAccess().getRightParenthesisKeyword_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__4__Impl"


    // $ANTLR start "rule__Rule__Group__5"
    // InternalPolicyDsl.g:870:1: rule__Rule__Group__5 : rule__Rule__Group__5__Impl rule__Rule__Group__6 ;
    public final void rule__Rule__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:874:1: ( rule__Rule__Group__5__Impl rule__Rule__Group__6 )
            // InternalPolicyDsl.g:875:2: rule__Rule__Group__5__Impl rule__Rule__Group__6
            {
            pushFollow(FOLLOW_12);
            rule__Rule__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__5"


    // $ANTLR start "rule__Rule__Group__5__Impl"
    // InternalPolicyDsl.g:882:1: rule__Rule__Group__5__Impl : ( '->' ) ;
    public final void rule__Rule__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:886:1: ( ( '->' ) )
            // InternalPolicyDsl.g:887:1: ( '->' )
            {
            // InternalPolicyDsl.g:887:1: ( '->' )
            // InternalPolicyDsl.g:888:2: '->'
            {
             before(grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_5()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__5__Impl"


    // $ANTLR start "rule__Rule__Group__6"
    // InternalPolicyDsl.g:897:1: rule__Rule__Group__6 : rule__Rule__Group__6__Impl ;
    public final void rule__Rule__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:901:1: ( rule__Rule__Group__6__Impl )
            // InternalPolicyDsl.g:902:2: rule__Rule__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__6"


    // $ANTLR start "rule__Rule__Group__6__Impl"
    // InternalPolicyDsl.g:908:1: rule__Rule__Group__6__Impl : ( ( rule__Rule__RhsAssignment_6 ) ) ;
    public final void rule__Rule__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:912:1: ( ( ( rule__Rule__RhsAssignment_6 ) ) )
            // InternalPolicyDsl.g:913:1: ( ( rule__Rule__RhsAssignment_6 ) )
            {
            // InternalPolicyDsl.g:913:1: ( ( rule__Rule__RhsAssignment_6 ) )
            // InternalPolicyDsl.g:914:2: ( rule__Rule__RhsAssignment_6 )
            {
             before(grammarAccess.getRuleAccess().getRhsAssignment_6()); 
            // InternalPolicyDsl.g:915:2: ( rule__Rule__RhsAssignment_6 )
            // InternalPolicyDsl.g:915:3: rule__Rule__RhsAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Rule__RhsAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getRhsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__6__Impl"


    // $ANTLR start "rule__LHS__Group__0"
    // InternalPolicyDsl.g:924:1: rule__LHS__Group__0 : rule__LHS__Group__0__Impl rule__LHS__Group__1 ;
    public final void rule__LHS__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:928:1: ( rule__LHS__Group__0__Impl rule__LHS__Group__1 )
            // InternalPolicyDsl.g:929:2: rule__LHS__Group__0__Impl rule__LHS__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__LHS__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__0"


    // $ANTLR start "rule__LHS__Group__0__Impl"
    // InternalPolicyDsl.g:936:1: rule__LHS__Group__0__Impl : ( 'Subject' ) ;
    public final void rule__LHS__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:940:1: ( ( 'Subject' ) )
            // InternalPolicyDsl.g:941:1: ( 'Subject' )
            {
            // InternalPolicyDsl.g:941:1: ( 'Subject' )
            // InternalPolicyDsl.g:942:2: 'Subject'
            {
             before(grammarAccess.getLHSAccess().getSubjectKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getSubjectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__0__Impl"


    // $ANTLR start "rule__LHS__Group__1"
    // InternalPolicyDsl.g:951:1: rule__LHS__Group__1 : rule__LHS__Group__1__Impl rule__LHS__Group__2 ;
    public final void rule__LHS__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:955:1: ( rule__LHS__Group__1__Impl rule__LHS__Group__2 )
            // InternalPolicyDsl.g:956:2: rule__LHS__Group__1__Impl rule__LHS__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__LHS__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__1"


    // $ANTLR start "rule__LHS__Group__1__Impl"
    // InternalPolicyDsl.g:963:1: rule__LHS__Group__1__Impl : ( ( rule__LHS__SubjectsAssignment_1 ) ) ;
    public final void rule__LHS__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:967:1: ( ( ( rule__LHS__SubjectsAssignment_1 ) ) )
            // InternalPolicyDsl.g:968:1: ( ( rule__LHS__SubjectsAssignment_1 ) )
            {
            // InternalPolicyDsl.g:968:1: ( ( rule__LHS__SubjectsAssignment_1 ) )
            // InternalPolicyDsl.g:969:2: ( rule__LHS__SubjectsAssignment_1 )
            {
             before(grammarAccess.getLHSAccess().getSubjectsAssignment_1()); 
            // InternalPolicyDsl.g:970:2: ( rule__LHS__SubjectsAssignment_1 )
            // InternalPolicyDsl.g:970:3: rule__LHS__SubjectsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__LHS__SubjectsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLHSAccess().getSubjectsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__1__Impl"


    // $ANTLR start "rule__LHS__Group__2"
    // InternalPolicyDsl.g:978:1: rule__LHS__Group__2 : rule__LHS__Group__2__Impl rule__LHS__Group__3 ;
    public final void rule__LHS__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:982:1: ( rule__LHS__Group__2__Impl rule__LHS__Group__3 )
            // InternalPolicyDsl.g:983:2: rule__LHS__Group__2__Impl rule__LHS__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__LHS__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__2"


    // $ANTLR start "rule__LHS__Group__2__Impl"
    // InternalPolicyDsl.g:990:1: rule__LHS__Group__2__Impl : ( ( rule__LHS__Group_2__0 )* ) ;
    public final void rule__LHS__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:994:1: ( ( ( rule__LHS__Group_2__0 )* ) )
            // InternalPolicyDsl.g:995:1: ( ( rule__LHS__Group_2__0 )* )
            {
            // InternalPolicyDsl.g:995:1: ( ( rule__LHS__Group_2__0 )* )
            // InternalPolicyDsl.g:996:2: ( rule__LHS__Group_2__0 )*
            {
             before(grammarAccess.getLHSAccess().getGroup_2()); 
            // InternalPolicyDsl.g:997:2: ( rule__LHS__Group_2__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==27) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPolicyDsl.g:997:3: rule__LHS__Group_2__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__LHS__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getLHSAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__2__Impl"


    // $ANTLR start "rule__LHS__Group__3"
    // InternalPolicyDsl.g:1005:1: rule__LHS__Group__3 : rule__LHS__Group__3__Impl rule__LHS__Group__4 ;
    public final void rule__LHS__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1009:1: ( rule__LHS__Group__3__Impl rule__LHS__Group__4 )
            // InternalPolicyDsl.g:1010:2: rule__LHS__Group__3__Impl rule__LHS__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__LHS__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__3"


    // $ANTLR start "rule__LHS__Group__3__Impl"
    // InternalPolicyDsl.g:1017:1: rule__LHS__Group__3__Impl : ( ';' ) ;
    public final void rule__LHS__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1021:1: ( ( ';' ) )
            // InternalPolicyDsl.g:1022:1: ( ';' )
            {
            // InternalPolicyDsl.g:1022:1: ( ';' )
            // InternalPolicyDsl.g:1023:2: ';'
            {
             before(grammarAccess.getLHSAccess().getSemicolonKeyword_3()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__3__Impl"


    // $ANTLR start "rule__LHS__Group__4"
    // InternalPolicyDsl.g:1032:1: rule__LHS__Group__4 : rule__LHS__Group__4__Impl rule__LHS__Group__5 ;
    public final void rule__LHS__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1036:1: ( rule__LHS__Group__4__Impl rule__LHS__Group__5 )
            // InternalPolicyDsl.g:1037:2: rule__LHS__Group__4__Impl rule__LHS__Group__5
            {
            pushFollow(FOLLOW_16);
            rule__LHS__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__4"


    // $ANTLR start "rule__LHS__Group__4__Impl"
    // InternalPolicyDsl.g:1044:1: rule__LHS__Group__4__Impl : ( 'Action' ) ;
    public final void rule__LHS__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1048:1: ( ( 'Action' ) )
            // InternalPolicyDsl.g:1049:1: ( 'Action' )
            {
            // InternalPolicyDsl.g:1049:1: ( 'Action' )
            // InternalPolicyDsl.g:1050:2: 'Action'
            {
             before(grammarAccess.getLHSAccess().getActionKeyword_4()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getActionKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__4__Impl"


    // $ANTLR start "rule__LHS__Group__5"
    // InternalPolicyDsl.g:1059:1: rule__LHS__Group__5 : rule__LHS__Group__5__Impl rule__LHS__Group__6 ;
    public final void rule__LHS__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1063:1: ( rule__LHS__Group__5__Impl rule__LHS__Group__6 )
            // InternalPolicyDsl.g:1064:2: rule__LHS__Group__5__Impl rule__LHS__Group__6
            {
            pushFollow(FOLLOW_13);
            rule__LHS__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__5"


    // $ANTLR start "rule__LHS__Group__5__Impl"
    // InternalPolicyDsl.g:1071:1: rule__LHS__Group__5__Impl : ( ( rule__LHS__ActionsAssignment_5 ) ) ;
    public final void rule__LHS__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1075:1: ( ( ( rule__LHS__ActionsAssignment_5 ) ) )
            // InternalPolicyDsl.g:1076:1: ( ( rule__LHS__ActionsAssignment_5 ) )
            {
            // InternalPolicyDsl.g:1076:1: ( ( rule__LHS__ActionsAssignment_5 ) )
            // InternalPolicyDsl.g:1077:2: ( rule__LHS__ActionsAssignment_5 )
            {
             before(grammarAccess.getLHSAccess().getActionsAssignment_5()); 
            // InternalPolicyDsl.g:1078:2: ( rule__LHS__ActionsAssignment_5 )
            // InternalPolicyDsl.g:1078:3: rule__LHS__ActionsAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__LHS__ActionsAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getLHSAccess().getActionsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__5__Impl"


    // $ANTLR start "rule__LHS__Group__6"
    // InternalPolicyDsl.g:1086:1: rule__LHS__Group__6 : rule__LHS__Group__6__Impl rule__LHS__Group__7 ;
    public final void rule__LHS__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1090:1: ( rule__LHS__Group__6__Impl rule__LHS__Group__7 )
            // InternalPolicyDsl.g:1091:2: rule__LHS__Group__6__Impl rule__LHS__Group__7
            {
            pushFollow(FOLLOW_13);
            rule__LHS__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__6"


    // $ANTLR start "rule__LHS__Group__6__Impl"
    // InternalPolicyDsl.g:1098:1: rule__LHS__Group__6__Impl : ( ( rule__LHS__Group_6__0 )* ) ;
    public final void rule__LHS__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1102:1: ( ( ( rule__LHS__Group_6__0 )* ) )
            // InternalPolicyDsl.g:1103:1: ( ( rule__LHS__Group_6__0 )* )
            {
            // InternalPolicyDsl.g:1103:1: ( ( rule__LHS__Group_6__0 )* )
            // InternalPolicyDsl.g:1104:2: ( rule__LHS__Group_6__0 )*
            {
             before(grammarAccess.getLHSAccess().getGroup_6()); 
            // InternalPolicyDsl.g:1105:2: ( rule__LHS__Group_6__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==27) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPolicyDsl.g:1105:3: rule__LHS__Group_6__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__LHS__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getLHSAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__6__Impl"


    // $ANTLR start "rule__LHS__Group__7"
    // InternalPolicyDsl.g:1113:1: rule__LHS__Group__7 : rule__LHS__Group__7__Impl rule__LHS__Group__8 ;
    public final void rule__LHS__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1117:1: ( rule__LHS__Group__7__Impl rule__LHS__Group__8 )
            // InternalPolicyDsl.g:1118:2: rule__LHS__Group__7__Impl rule__LHS__Group__8
            {
            pushFollow(FOLLOW_17);
            rule__LHS__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__7"


    // $ANTLR start "rule__LHS__Group__7__Impl"
    // InternalPolicyDsl.g:1125:1: rule__LHS__Group__7__Impl : ( ';' ) ;
    public final void rule__LHS__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1129:1: ( ( ';' ) )
            // InternalPolicyDsl.g:1130:1: ( ';' )
            {
            // InternalPolicyDsl.g:1130:1: ( ';' )
            // InternalPolicyDsl.g:1131:2: ';'
            {
             before(grammarAccess.getLHSAccess().getSemicolonKeyword_7()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__7__Impl"


    // $ANTLR start "rule__LHS__Group__8"
    // InternalPolicyDsl.g:1140:1: rule__LHS__Group__8 : rule__LHS__Group__8__Impl rule__LHS__Group__9 ;
    public final void rule__LHS__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1144:1: ( rule__LHS__Group__8__Impl rule__LHS__Group__9 )
            // InternalPolicyDsl.g:1145:2: rule__LHS__Group__8__Impl rule__LHS__Group__9
            {
            pushFollow(FOLLOW_18);
            rule__LHS__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__8"


    // $ANTLR start "rule__LHS__Group__8__Impl"
    // InternalPolicyDsl.g:1152:1: rule__LHS__Group__8__Impl : ( 'Object' ) ;
    public final void rule__LHS__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1156:1: ( ( 'Object' ) )
            // InternalPolicyDsl.g:1157:1: ( 'Object' )
            {
            // InternalPolicyDsl.g:1157:1: ( 'Object' )
            // InternalPolicyDsl.g:1158:2: 'Object'
            {
             before(grammarAccess.getLHSAccess().getObjectKeyword_8()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getObjectKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__8__Impl"


    // $ANTLR start "rule__LHS__Group__9"
    // InternalPolicyDsl.g:1167:1: rule__LHS__Group__9 : rule__LHS__Group__9__Impl ;
    public final void rule__LHS__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1171:1: ( rule__LHS__Group__9__Impl )
            // InternalPolicyDsl.g:1172:2: rule__LHS__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LHS__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__9"


    // $ANTLR start "rule__LHS__Group__9__Impl"
    // InternalPolicyDsl.g:1178:1: rule__LHS__Group__9__Impl : ( ( rule__LHS__ObjectConditionAssignment_9 ) ) ;
    public final void rule__LHS__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1182:1: ( ( ( rule__LHS__ObjectConditionAssignment_9 ) ) )
            // InternalPolicyDsl.g:1183:1: ( ( rule__LHS__ObjectConditionAssignment_9 ) )
            {
            // InternalPolicyDsl.g:1183:1: ( ( rule__LHS__ObjectConditionAssignment_9 ) )
            // InternalPolicyDsl.g:1184:2: ( rule__LHS__ObjectConditionAssignment_9 )
            {
             before(grammarAccess.getLHSAccess().getObjectConditionAssignment_9()); 
            // InternalPolicyDsl.g:1185:2: ( rule__LHS__ObjectConditionAssignment_9 )
            // InternalPolicyDsl.g:1185:3: rule__LHS__ObjectConditionAssignment_9
            {
            pushFollow(FOLLOW_2);
            rule__LHS__ObjectConditionAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getLHSAccess().getObjectConditionAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group__9__Impl"


    // $ANTLR start "rule__LHS__Group_2__0"
    // InternalPolicyDsl.g:1194:1: rule__LHS__Group_2__0 : rule__LHS__Group_2__0__Impl rule__LHS__Group_2__1 ;
    public final void rule__LHS__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1198:1: ( rule__LHS__Group_2__0__Impl rule__LHS__Group_2__1 )
            // InternalPolicyDsl.g:1199:2: rule__LHS__Group_2__0__Impl rule__LHS__Group_2__1
            {
            pushFollow(FOLLOW_6);
            rule__LHS__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_2__0"


    // $ANTLR start "rule__LHS__Group_2__0__Impl"
    // InternalPolicyDsl.g:1206:1: rule__LHS__Group_2__0__Impl : ( ',' ) ;
    public final void rule__LHS__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1210:1: ( ( ',' ) )
            // InternalPolicyDsl.g:1211:1: ( ',' )
            {
            // InternalPolicyDsl.g:1211:1: ( ',' )
            // InternalPolicyDsl.g:1212:2: ','
            {
             before(grammarAccess.getLHSAccess().getCommaKeyword_2_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_2__0__Impl"


    // $ANTLR start "rule__LHS__Group_2__1"
    // InternalPolicyDsl.g:1221:1: rule__LHS__Group_2__1 : rule__LHS__Group_2__1__Impl ;
    public final void rule__LHS__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1225:1: ( rule__LHS__Group_2__1__Impl )
            // InternalPolicyDsl.g:1226:2: rule__LHS__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LHS__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_2__1"


    // $ANTLR start "rule__LHS__Group_2__1__Impl"
    // InternalPolicyDsl.g:1232:1: rule__LHS__Group_2__1__Impl : ( ( rule__LHS__SubjectsAssignment_2_1 ) ) ;
    public final void rule__LHS__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1236:1: ( ( ( rule__LHS__SubjectsAssignment_2_1 ) ) )
            // InternalPolicyDsl.g:1237:1: ( ( rule__LHS__SubjectsAssignment_2_1 ) )
            {
            // InternalPolicyDsl.g:1237:1: ( ( rule__LHS__SubjectsAssignment_2_1 ) )
            // InternalPolicyDsl.g:1238:2: ( rule__LHS__SubjectsAssignment_2_1 )
            {
             before(grammarAccess.getLHSAccess().getSubjectsAssignment_2_1()); 
            // InternalPolicyDsl.g:1239:2: ( rule__LHS__SubjectsAssignment_2_1 )
            // InternalPolicyDsl.g:1239:3: rule__LHS__SubjectsAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__LHS__SubjectsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLHSAccess().getSubjectsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_2__1__Impl"


    // $ANTLR start "rule__LHS__Group_6__0"
    // InternalPolicyDsl.g:1248:1: rule__LHS__Group_6__0 : rule__LHS__Group_6__0__Impl rule__LHS__Group_6__1 ;
    public final void rule__LHS__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1252:1: ( rule__LHS__Group_6__0__Impl rule__LHS__Group_6__1 )
            // InternalPolicyDsl.g:1253:2: rule__LHS__Group_6__0__Impl rule__LHS__Group_6__1
            {
            pushFollow(FOLLOW_16);
            rule__LHS__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LHS__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_6__0"


    // $ANTLR start "rule__LHS__Group_6__0__Impl"
    // InternalPolicyDsl.g:1260:1: rule__LHS__Group_6__0__Impl : ( ',' ) ;
    public final void rule__LHS__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1264:1: ( ( ',' ) )
            // InternalPolicyDsl.g:1265:1: ( ',' )
            {
            // InternalPolicyDsl.g:1265:1: ( ',' )
            // InternalPolicyDsl.g:1266:2: ','
            {
             before(grammarAccess.getLHSAccess().getCommaKeyword_6_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getLHSAccess().getCommaKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_6__0__Impl"


    // $ANTLR start "rule__LHS__Group_6__1"
    // InternalPolicyDsl.g:1275:1: rule__LHS__Group_6__1 : rule__LHS__Group_6__1__Impl ;
    public final void rule__LHS__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1279:1: ( rule__LHS__Group_6__1__Impl )
            // InternalPolicyDsl.g:1280:2: rule__LHS__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LHS__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_6__1"


    // $ANTLR start "rule__LHS__Group_6__1__Impl"
    // InternalPolicyDsl.g:1286:1: rule__LHS__Group_6__1__Impl : ( ( rule__LHS__ActionsAssignment_6_1 ) ) ;
    public final void rule__LHS__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1290:1: ( ( ( rule__LHS__ActionsAssignment_6_1 ) ) )
            // InternalPolicyDsl.g:1291:1: ( ( rule__LHS__ActionsAssignment_6_1 ) )
            {
            // InternalPolicyDsl.g:1291:1: ( ( rule__LHS__ActionsAssignment_6_1 ) )
            // InternalPolicyDsl.g:1292:2: ( rule__LHS__ActionsAssignment_6_1 )
            {
             before(grammarAccess.getLHSAccess().getActionsAssignment_6_1()); 
            // InternalPolicyDsl.g:1293:2: ( rule__LHS__ActionsAssignment_6_1 )
            // InternalPolicyDsl.g:1293:3: rule__LHS__ActionsAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__LHS__ActionsAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getLHSAccess().getActionsAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__Group_6__1__Impl"


    // $ANTLR start "rule__Subject__Group__0"
    // InternalPolicyDsl.g:1302:1: rule__Subject__Group__0 : rule__Subject__Group__0__Impl rule__Subject__Group__1 ;
    public final void rule__Subject__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1306:1: ( rule__Subject__Group__0__Impl rule__Subject__Group__1 )
            // InternalPolicyDsl.g:1307:2: rule__Subject__Group__0__Impl rule__Subject__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Subject__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Subject__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subject__Group__0"


    // $ANTLR start "rule__Subject__Group__0__Impl"
    // InternalPolicyDsl.g:1314:1: rule__Subject__Group__0__Impl : ( () ) ;
    public final void rule__Subject__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1318:1: ( ( () ) )
            // InternalPolicyDsl.g:1319:1: ( () )
            {
            // InternalPolicyDsl.g:1319:1: ( () )
            // InternalPolicyDsl.g:1320:2: ()
            {
             before(grammarAccess.getSubjectAccess().getSubjectAction_0()); 
            // InternalPolicyDsl.g:1321:2: ()
            // InternalPolicyDsl.g:1321:3: 
            {
            }

             after(grammarAccess.getSubjectAccess().getSubjectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subject__Group__0__Impl"


    // $ANTLR start "rule__Subject__Group__1"
    // InternalPolicyDsl.g:1329:1: rule__Subject__Group__1 : rule__Subject__Group__1__Impl ;
    public final void rule__Subject__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1333:1: ( rule__Subject__Group__1__Impl )
            // InternalPolicyDsl.g:1334:2: rule__Subject__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Subject__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subject__Group__1"


    // $ANTLR start "rule__Subject__Group__1__Impl"
    // InternalPolicyDsl.g:1340:1: rule__Subject__Group__1__Impl : ( ( rule__Subject__NameAssignment_1 ) ) ;
    public final void rule__Subject__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1344:1: ( ( ( rule__Subject__NameAssignment_1 ) ) )
            // InternalPolicyDsl.g:1345:1: ( ( rule__Subject__NameAssignment_1 ) )
            {
            // InternalPolicyDsl.g:1345:1: ( ( rule__Subject__NameAssignment_1 ) )
            // InternalPolicyDsl.g:1346:2: ( rule__Subject__NameAssignment_1 )
            {
             before(grammarAccess.getSubjectAccess().getNameAssignment_1()); 
            // InternalPolicyDsl.g:1347:2: ( rule__Subject__NameAssignment_1 )
            // InternalPolicyDsl.g:1347:3: rule__Subject__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Subject__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSubjectAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subject__Group__1__Impl"


    // $ANTLR start "rule__Action__Group__0"
    // InternalPolicyDsl.g:1356:1: rule__Action__Group__0 : rule__Action__Group__0__Impl rule__Action__Group__1 ;
    public final void rule__Action__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1360:1: ( rule__Action__Group__0__Impl rule__Action__Group__1 )
            // InternalPolicyDsl.g:1361:2: rule__Action__Group__0__Impl rule__Action__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Action__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0"


    // $ANTLR start "rule__Action__Group__0__Impl"
    // InternalPolicyDsl.g:1368:1: rule__Action__Group__0__Impl : ( () ) ;
    public final void rule__Action__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1372:1: ( ( () ) )
            // InternalPolicyDsl.g:1373:1: ( () )
            {
            // InternalPolicyDsl.g:1373:1: ( () )
            // InternalPolicyDsl.g:1374:2: ()
            {
             before(grammarAccess.getActionAccess().getActionAction_0()); 
            // InternalPolicyDsl.g:1375:2: ()
            // InternalPolicyDsl.g:1375:3: 
            {
            }

             after(grammarAccess.getActionAccess().getActionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0__Impl"


    // $ANTLR start "rule__Action__Group__1"
    // InternalPolicyDsl.g:1383:1: rule__Action__Group__1 : rule__Action__Group__1__Impl ;
    public final void rule__Action__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1387:1: ( rule__Action__Group__1__Impl )
            // InternalPolicyDsl.g:1388:2: rule__Action__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1"


    // $ANTLR start "rule__Action__Group__1__Impl"
    // InternalPolicyDsl.g:1394:1: rule__Action__Group__1__Impl : ( ( rule__Action__ActionAssignment_1 ) ) ;
    public final void rule__Action__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1398:1: ( ( ( rule__Action__ActionAssignment_1 ) ) )
            // InternalPolicyDsl.g:1399:1: ( ( rule__Action__ActionAssignment_1 ) )
            {
            // InternalPolicyDsl.g:1399:1: ( ( rule__Action__ActionAssignment_1 ) )
            // InternalPolicyDsl.g:1400:2: ( rule__Action__ActionAssignment_1 )
            {
             before(grammarAccess.getActionAccess().getActionAssignment_1()); 
            // InternalPolicyDsl.g:1401:2: ( rule__Action__ActionAssignment_1 )
            // InternalPolicyDsl.g:1401:3: rule__Action__ActionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Action__ActionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getActionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1__Impl"


    // $ANTLR start "rule__ClassCondition__Group__0"
    // InternalPolicyDsl.g:1410:1: rule__ClassCondition__Group__0 : rule__ClassCondition__Group__0__Impl rule__ClassCondition__Group__1 ;
    public final void rule__ClassCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1414:1: ( rule__ClassCondition__Group__0__Impl rule__ClassCondition__Group__1 )
            // InternalPolicyDsl.g:1415:2: rule__ClassCondition__Group__0__Impl rule__ClassCondition__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__ClassCondition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group__0"


    // $ANTLR start "rule__ClassCondition__Group__0__Impl"
    // InternalPolicyDsl.g:1422:1: rule__ClassCondition__Group__0__Impl : ( 'class' ) ;
    public final void rule__ClassCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1426:1: ( ( 'class' ) )
            // InternalPolicyDsl.g:1427:1: ( 'class' )
            {
            // InternalPolicyDsl.g:1427:1: ( 'class' )
            // InternalPolicyDsl.g:1428:2: 'class'
            {
             before(grammarAccess.getClassConditionAccess().getClassKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getClassConditionAccess().getClassKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group__0__Impl"


    // $ANTLR start "rule__ClassCondition__Group__1"
    // InternalPolicyDsl.g:1437:1: rule__ClassCondition__Group__1 : rule__ClassCondition__Group__1__Impl rule__ClassCondition__Group__2 ;
    public final void rule__ClassCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1441:1: ( rule__ClassCondition__Group__1__Impl rule__ClassCondition__Group__2 )
            // InternalPolicyDsl.g:1442:2: rule__ClassCondition__Group__1__Impl rule__ClassCondition__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__ClassCondition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group__1"


    // $ANTLR start "rule__ClassCondition__Group__1__Impl"
    // InternalPolicyDsl.g:1449:1: rule__ClassCondition__Group__1__Impl : ( ( rule__ClassCondition__ClassCondRefAssignment_1 ) ) ;
    public final void rule__ClassCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1453:1: ( ( ( rule__ClassCondition__ClassCondRefAssignment_1 ) ) )
            // InternalPolicyDsl.g:1454:1: ( ( rule__ClassCondition__ClassCondRefAssignment_1 ) )
            {
            // InternalPolicyDsl.g:1454:1: ( ( rule__ClassCondition__ClassCondRefAssignment_1 ) )
            // InternalPolicyDsl.g:1455:2: ( rule__ClassCondition__ClassCondRefAssignment_1 )
            {
             before(grammarAccess.getClassConditionAccess().getClassCondRefAssignment_1()); 
            // InternalPolicyDsl.g:1456:2: ( rule__ClassCondition__ClassCondRefAssignment_1 )
            // InternalPolicyDsl.g:1456:3: rule__ClassCondition__ClassCondRefAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassCondition__ClassCondRefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getClassConditionAccess().getClassCondRefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group__1__Impl"


    // $ANTLR start "rule__ClassCondition__Group__2"
    // InternalPolicyDsl.g:1464:1: rule__ClassCondition__Group__2 : rule__ClassCondition__Group__2__Impl ;
    public final void rule__ClassCondition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1468:1: ( rule__ClassCondition__Group__2__Impl )
            // InternalPolicyDsl.g:1469:2: rule__ClassCondition__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group__2"


    // $ANTLR start "rule__ClassCondition__Group__2__Impl"
    // InternalPolicyDsl.g:1475:1: rule__ClassCondition__Group__2__Impl : ( ( rule__ClassCondition__Group_2__0 )? ) ;
    public final void rule__ClassCondition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1479:1: ( ( ( rule__ClassCondition__Group_2__0 )? ) )
            // InternalPolicyDsl.g:1480:1: ( ( rule__ClassCondition__Group_2__0 )? )
            {
            // InternalPolicyDsl.g:1480:1: ( ( rule__ClassCondition__Group_2__0 )? )
            // InternalPolicyDsl.g:1481:2: ( rule__ClassCondition__Group_2__0 )?
            {
             before(grammarAccess.getClassConditionAccess().getGroup_2()); 
            // InternalPolicyDsl.g:1482:2: ( rule__ClassCondition__Group_2__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==29) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalPolicyDsl.g:1482:3: rule__ClassCondition__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ClassCondition__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getClassConditionAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group__2__Impl"


    // $ANTLR start "rule__ClassCondition__Group_2__0"
    // InternalPolicyDsl.g:1491:1: rule__ClassCondition__Group_2__0 : rule__ClassCondition__Group_2__0__Impl rule__ClassCondition__Group_2__1 ;
    public final void rule__ClassCondition__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1495:1: ( rule__ClassCondition__Group_2__0__Impl rule__ClassCondition__Group_2__1 )
            // InternalPolicyDsl.g:1496:2: rule__ClassCondition__Group_2__0__Impl rule__ClassCondition__Group_2__1
            {
            pushFollow(FOLLOW_20);
            rule__ClassCondition__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__0"


    // $ANTLR start "rule__ClassCondition__Group_2__0__Impl"
    // InternalPolicyDsl.g:1503:1: rule__ClassCondition__Group_2__0__Impl : ( 'WithValue' ) ;
    public final void rule__ClassCondition__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1507:1: ( ( 'WithValue' ) )
            // InternalPolicyDsl.g:1508:1: ( 'WithValue' )
            {
            // InternalPolicyDsl.g:1508:1: ( 'WithValue' )
            // InternalPolicyDsl.g:1509:2: 'WithValue'
            {
             before(grammarAccess.getClassConditionAccess().getWithValueKeyword_2_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getClassConditionAccess().getWithValueKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__0__Impl"


    // $ANTLR start "rule__ClassCondition__Group_2__1"
    // InternalPolicyDsl.g:1518:1: rule__ClassCondition__Group_2__1 : rule__ClassCondition__Group_2__1__Impl rule__ClassCondition__Group_2__2 ;
    public final void rule__ClassCondition__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1522:1: ( rule__ClassCondition__Group_2__1__Impl rule__ClassCondition__Group_2__2 )
            // InternalPolicyDsl.g:1523:2: rule__ClassCondition__Group_2__1__Impl rule__ClassCondition__Group_2__2
            {
            pushFollow(FOLLOW_21);
            rule__ClassCondition__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__1"


    // $ANTLR start "rule__ClassCondition__Group_2__1__Impl"
    // InternalPolicyDsl.g:1530:1: rule__ClassCondition__Group_2__1__Impl : ( '=' ) ;
    public final void rule__ClassCondition__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1534:1: ( ( '=' ) )
            // InternalPolicyDsl.g:1535:1: ( '=' )
            {
            // InternalPolicyDsl.g:1535:1: ( '=' )
            // InternalPolicyDsl.g:1536:2: '='
            {
             before(grammarAccess.getClassConditionAccess().getEqualsSignKeyword_2_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getClassConditionAccess().getEqualsSignKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__1__Impl"


    // $ANTLR start "rule__ClassCondition__Group_2__2"
    // InternalPolicyDsl.g:1545:1: rule__ClassCondition__Group_2__2 : rule__ClassCondition__Group_2__2__Impl rule__ClassCondition__Group_2__3 ;
    public final void rule__ClassCondition__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1549:1: ( rule__ClassCondition__Group_2__2__Impl rule__ClassCondition__Group_2__3 )
            // InternalPolicyDsl.g:1550:2: rule__ClassCondition__Group_2__2__Impl rule__ClassCondition__Group_2__3
            {
            pushFollow(FOLLOW_7);
            rule__ClassCondition__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__2"


    // $ANTLR start "rule__ClassCondition__Group_2__2__Impl"
    // InternalPolicyDsl.g:1557:1: rule__ClassCondition__Group_2__2__Impl : ( '<' ) ;
    public final void rule__ClassCondition__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1561:1: ( ( '<' ) )
            // InternalPolicyDsl.g:1562:1: ( '<' )
            {
            // InternalPolicyDsl.g:1562:1: ( '<' )
            // InternalPolicyDsl.g:1563:2: '<'
            {
             before(grammarAccess.getClassConditionAccess().getLessThanSignKeyword_2_2()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getClassConditionAccess().getLessThanSignKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__2__Impl"


    // $ANTLR start "rule__ClassCondition__Group_2__3"
    // InternalPolicyDsl.g:1572:1: rule__ClassCondition__Group_2__3 : rule__ClassCondition__Group_2__3__Impl rule__ClassCondition__Group_2__4 ;
    public final void rule__ClassCondition__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1576:1: ( rule__ClassCondition__Group_2__3__Impl rule__ClassCondition__Group_2__4 )
            // InternalPolicyDsl.g:1577:2: rule__ClassCondition__Group_2__3__Impl rule__ClassCondition__Group_2__4
            {
            pushFollow(FOLLOW_22);
            rule__ClassCondition__Group_2__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group_2__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__3"


    // $ANTLR start "rule__ClassCondition__Group_2__3__Impl"
    // InternalPolicyDsl.g:1584:1: rule__ClassCondition__Group_2__3__Impl : ( ( rule__ClassCondition__ValueAssignment_2_3 ) ) ;
    public final void rule__ClassCondition__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1588:1: ( ( ( rule__ClassCondition__ValueAssignment_2_3 ) ) )
            // InternalPolicyDsl.g:1589:1: ( ( rule__ClassCondition__ValueAssignment_2_3 ) )
            {
            // InternalPolicyDsl.g:1589:1: ( ( rule__ClassCondition__ValueAssignment_2_3 ) )
            // InternalPolicyDsl.g:1590:2: ( rule__ClassCondition__ValueAssignment_2_3 )
            {
             before(grammarAccess.getClassConditionAccess().getValueAssignment_2_3()); 
            // InternalPolicyDsl.g:1591:2: ( rule__ClassCondition__ValueAssignment_2_3 )
            // InternalPolicyDsl.g:1591:3: rule__ClassCondition__ValueAssignment_2_3
            {
            pushFollow(FOLLOW_2);
            rule__ClassCondition__ValueAssignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getClassConditionAccess().getValueAssignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__3__Impl"


    // $ANTLR start "rule__ClassCondition__Group_2__4"
    // InternalPolicyDsl.g:1599:1: rule__ClassCondition__Group_2__4 : rule__ClassCondition__Group_2__4__Impl ;
    public final void rule__ClassCondition__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1603:1: ( rule__ClassCondition__Group_2__4__Impl )
            // InternalPolicyDsl.g:1604:2: rule__ClassCondition__Group_2__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassCondition__Group_2__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__4"


    // $ANTLR start "rule__ClassCondition__Group_2__4__Impl"
    // InternalPolicyDsl.g:1610:1: rule__ClassCondition__Group_2__4__Impl : ( '>' ) ;
    public final void rule__ClassCondition__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1614:1: ( ( '>' ) )
            // InternalPolicyDsl.g:1615:1: ( '>' )
            {
            // InternalPolicyDsl.g:1615:1: ( '>' )
            // InternalPolicyDsl.g:1616:2: '>'
            {
             before(grammarAccess.getClassConditionAccess().getGreaterThanSignKeyword_2_4()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getClassConditionAccess().getGreaterThanSignKeyword_2_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__Group_2__4__Impl"


    // $ANTLR start "rule__ReferenceCondition__Group__0"
    // InternalPolicyDsl.g:1626:1: rule__ReferenceCondition__Group__0 : rule__ReferenceCondition__Group__0__Impl rule__ReferenceCondition__Group__1 ;
    public final void rule__ReferenceCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1630:1: ( rule__ReferenceCondition__Group__0__Impl rule__ReferenceCondition__Group__1 )
            // InternalPolicyDsl.g:1631:2: rule__ReferenceCondition__Group__0__Impl rule__ReferenceCondition__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__ReferenceCondition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReferenceCondition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceCondition__Group__0"


    // $ANTLR start "rule__ReferenceCondition__Group__0__Impl"
    // InternalPolicyDsl.g:1638:1: rule__ReferenceCondition__Group__0__Impl : ( 'ref' ) ;
    public final void rule__ReferenceCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1642:1: ( ( 'ref' ) )
            // InternalPolicyDsl.g:1643:1: ( 'ref' )
            {
            // InternalPolicyDsl.g:1643:1: ( 'ref' )
            // InternalPolicyDsl.g:1644:2: 'ref'
            {
             before(grammarAccess.getReferenceConditionAccess().getRefKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getReferenceConditionAccess().getRefKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceCondition__Group__0__Impl"


    // $ANTLR start "rule__ReferenceCondition__Group__1"
    // InternalPolicyDsl.g:1653:1: rule__ReferenceCondition__Group__1 : rule__ReferenceCondition__Group__1__Impl ;
    public final void rule__ReferenceCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1657:1: ( rule__ReferenceCondition__Group__1__Impl )
            // InternalPolicyDsl.g:1658:2: rule__ReferenceCondition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceCondition__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceCondition__Group__1"


    // $ANTLR start "rule__ReferenceCondition__Group__1__Impl"
    // InternalPolicyDsl.g:1664:1: rule__ReferenceCondition__Group__1__Impl : ( ( rule__ReferenceCondition__FeatureCondRefAssignment_1 ) ) ;
    public final void rule__ReferenceCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1668:1: ( ( ( rule__ReferenceCondition__FeatureCondRefAssignment_1 ) ) )
            // InternalPolicyDsl.g:1669:1: ( ( rule__ReferenceCondition__FeatureCondRefAssignment_1 ) )
            {
            // InternalPolicyDsl.g:1669:1: ( ( rule__ReferenceCondition__FeatureCondRefAssignment_1 ) )
            // InternalPolicyDsl.g:1670:2: ( rule__ReferenceCondition__FeatureCondRefAssignment_1 )
            {
             before(grammarAccess.getReferenceConditionAccess().getFeatureCondRefAssignment_1()); 
            // InternalPolicyDsl.g:1671:2: ( rule__ReferenceCondition__FeatureCondRefAssignment_1 )
            // InternalPolicyDsl.g:1671:3: rule__ReferenceCondition__FeatureCondRefAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceCondition__FeatureCondRefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReferenceConditionAccess().getFeatureCondRefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceCondition__Group__1__Impl"


    // $ANTLR start "rule__FeatureCondition__Group__0"
    // InternalPolicyDsl.g:1680:1: rule__FeatureCondition__Group__0 : rule__FeatureCondition__Group__0__Impl rule__FeatureCondition__Group__1 ;
    public final void rule__FeatureCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1684:1: ( rule__FeatureCondition__Group__0__Impl rule__FeatureCondition__Group__1 )
            // InternalPolicyDsl.g:1685:2: rule__FeatureCondition__Group__0__Impl rule__FeatureCondition__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__FeatureCondition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureCondition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCondition__Group__0"


    // $ANTLR start "rule__FeatureCondition__Group__0__Impl"
    // InternalPolicyDsl.g:1692:1: rule__FeatureCondition__Group__0__Impl : ( 'att' ) ;
    public final void rule__FeatureCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1696:1: ( ( 'att' ) )
            // InternalPolicyDsl.g:1697:1: ( 'att' )
            {
            // InternalPolicyDsl.g:1697:1: ( 'att' )
            // InternalPolicyDsl.g:1698:2: 'att'
            {
             before(grammarAccess.getFeatureConditionAccess().getAttKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getFeatureConditionAccess().getAttKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCondition__Group__0__Impl"


    // $ANTLR start "rule__FeatureCondition__Group__1"
    // InternalPolicyDsl.g:1707:1: rule__FeatureCondition__Group__1 : rule__FeatureCondition__Group__1__Impl ;
    public final void rule__FeatureCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1711:1: ( rule__FeatureCondition__Group__1__Impl )
            // InternalPolicyDsl.g:1712:2: rule__FeatureCondition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FeatureCondition__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCondition__Group__1"


    // $ANTLR start "rule__FeatureCondition__Group__1__Impl"
    // InternalPolicyDsl.g:1718:1: rule__FeatureCondition__Group__1__Impl : ( ( rule__FeatureCondition__FeatureCondRefAssignment_1 ) ) ;
    public final void rule__FeatureCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1722:1: ( ( ( rule__FeatureCondition__FeatureCondRefAssignment_1 ) ) )
            // InternalPolicyDsl.g:1723:1: ( ( rule__FeatureCondition__FeatureCondRefAssignment_1 ) )
            {
            // InternalPolicyDsl.g:1723:1: ( ( rule__FeatureCondition__FeatureCondRefAssignment_1 ) )
            // InternalPolicyDsl.g:1724:2: ( rule__FeatureCondition__FeatureCondRefAssignment_1 )
            {
             before(grammarAccess.getFeatureConditionAccess().getFeatureCondRefAssignment_1()); 
            // InternalPolicyDsl.g:1725:2: ( rule__FeatureCondition__FeatureCondRefAssignment_1 )
            // InternalPolicyDsl.g:1725:3: rule__FeatureCondition__FeatureCondRefAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__FeatureCondition__FeatureCondRefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureConditionAccess().getFeatureCondRefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCondition__Group__1__Impl"


    // $ANTLR start "rule__OperationCondition__Group__0"
    // InternalPolicyDsl.g:1734:1: rule__OperationCondition__Group__0 : rule__OperationCondition__Group__0__Impl rule__OperationCondition__Group__1 ;
    public final void rule__OperationCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1738:1: ( rule__OperationCondition__Group__0__Impl rule__OperationCondition__Group__1 )
            // InternalPolicyDsl.g:1739:2: rule__OperationCondition__Group__0__Impl rule__OperationCondition__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__OperationCondition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OperationCondition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperationCondition__Group__0"


    // $ANTLR start "rule__OperationCondition__Group__0__Impl"
    // InternalPolicyDsl.g:1746:1: rule__OperationCondition__Group__0__Impl : ( 'op' ) ;
    public final void rule__OperationCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1750:1: ( ( 'op' ) )
            // InternalPolicyDsl.g:1751:1: ( 'op' )
            {
            // InternalPolicyDsl.g:1751:1: ( 'op' )
            // InternalPolicyDsl.g:1752:2: 'op'
            {
             before(grammarAccess.getOperationConditionAccess().getOpKeyword_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getOperationConditionAccess().getOpKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperationCondition__Group__0__Impl"


    // $ANTLR start "rule__OperationCondition__Group__1"
    // InternalPolicyDsl.g:1761:1: rule__OperationCondition__Group__1 : rule__OperationCondition__Group__1__Impl ;
    public final void rule__OperationCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1765:1: ( rule__OperationCondition__Group__1__Impl )
            // InternalPolicyDsl.g:1766:2: rule__OperationCondition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OperationCondition__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperationCondition__Group__1"


    // $ANTLR start "rule__OperationCondition__Group__1__Impl"
    // InternalPolicyDsl.g:1772:1: rule__OperationCondition__Group__1__Impl : ( ( rule__OperationCondition__OperationCondRefAssignment_1 ) ) ;
    public final void rule__OperationCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1776:1: ( ( ( rule__OperationCondition__OperationCondRefAssignment_1 ) ) )
            // InternalPolicyDsl.g:1777:1: ( ( rule__OperationCondition__OperationCondRefAssignment_1 ) )
            {
            // InternalPolicyDsl.g:1777:1: ( ( rule__OperationCondition__OperationCondRefAssignment_1 ) )
            // InternalPolicyDsl.g:1778:2: ( rule__OperationCondition__OperationCondRefAssignment_1 )
            {
             before(grammarAccess.getOperationConditionAccess().getOperationCondRefAssignment_1()); 
            // InternalPolicyDsl.g:1779:2: ( rule__OperationCondition__OperationCondRefAssignment_1 )
            // InternalPolicyDsl.g:1779:3: rule__OperationCondition__OperationCondRefAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__OperationCondition__OperationCondRefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOperationConditionAccess().getOperationCondRefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperationCondition__Group__1__Impl"


    // $ANTLR start "rule__FQN__Group__0"
    // InternalPolicyDsl.g:1788:1: rule__FQN__Group__0 : rule__FQN__Group__0__Impl rule__FQN__Group__1 ;
    public final void rule__FQN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1792:1: ( rule__FQN__Group__0__Impl rule__FQN__Group__1 )
            // InternalPolicyDsl.g:1793:2: rule__FQN__Group__0__Impl rule__FQN__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__FQN__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQN__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0"


    // $ANTLR start "rule__FQN__Group__0__Impl"
    // InternalPolicyDsl.g:1800:1: rule__FQN__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1804:1: ( ( RULE_ID ) )
            // InternalPolicyDsl.g:1805:1: ( RULE_ID )
            {
            // InternalPolicyDsl.g:1805:1: ( RULE_ID )
            // InternalPolicyDsl.g:1806:2: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0__Impl"


    // $ANTLR start "rule__FQN__Group__1"
    // InternalPolicyDsl.g:1815:1: rule__FQN__Group__1 : rule__FQN__Group__1__Impl ;
    public final void rule__FQN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1819:1: ( rule__FQN__Group__1__Impl )
            // InternalPolicyDsl.g:1820:2: rule__FQN__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1"


    // $ANTLR start "rule__FQN__Group__1__Impl"
    // InternalPolicyDsl.g:1826:1: rule__FQN__Group__1__Impl : ( ( rule__FQN__Group_1__0 )* ) ;
    public final void rule__FQN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1830:1: ( ( ( rule__FQN__Group_1__0 )* ) )
            // InternalPolicyDsl.g:1831:1: ( ( rule__FQN__Group_1__0 )* )
            {
            // InternalPolicyDsl.g:1831:1: ( ( rule__FQN__Group_1__0 )* )
            // InternalPolicyDsl.g:1832:2: ( rule__FQN__Group_1__0 )*
            {
             before(grammarAccess.getFQNAccess().getGroup_1()); 
            // InternalPolicyDsl.g:1833:2: ( rule__FQN__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==36) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalPolicyDsl.g:1833:3: rule__FQN__Group_1__0
            	    {
            	    pushFollow(FOLLOW_24);
            	    rule__FQN__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getFQNAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1__Impl"


    // $ANTLR start "rule__FQN__Group_1__0"
    // InternalPolicyDsl.g:1842:1: rule__FQN__Group_1__0 : rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 ;
    public final void rule__FQN__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1846:1: ( rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 )
            // InternalPolicyDsl.g:1847:2: rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__FQN__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQN__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0"


    // $ANTLR start "rule__FQN__Group_1__0__Impl"
    // InternalPolicyDsl.g:1854:1: rule__FQN__Group_1__0__Impl : ( '.' ) ;
    public final void rule__FQN__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1858:1: ( ( '.' ) )
            // InternalPolicyDsl.g:1859:1: ( '.' )
            {
            // InternalPolicyDsl.g:1859:1: ( '.' )
            // InternalPolicyDsl.g:1860:2: '.'
            {
             before(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0__Impl"


    // $ANTLR start "rule__FQN__Group_1__1"
    // InternalPolicyDsl.g:1869:1: rule__FQN__Group_1__1 : rule__FQN__Group_1__1__Impl ;
    public final void rule__FQN__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1873:1: ( rule__FQN__Group_1__1__Impl )
            // InternalPolicyDsl.g:1874:2: rule__FQN__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1"


    // $ANTLR start "rule__FQN__Group_1__1__Impl"
    // InternalPolicyDsl.g:1880:1: rule__FQN__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1884:1: ( ( RULE_ID ) )
            // InternalPolicyDsl.g:1885:1: ( RULE_ID )
            {
            // InternalPolicyDsl.g:1885:1: ( RULE_ID )
            // InternalPolicyDsl.g:1886:2: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1__Impl"


    // $ANTLR start "rule__Policy__ImportsAssignment_0"
    // InternalPolicyDsl.g:1896:1: rule__Policy__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Policy__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1900:1: ( ( ruleImport ) )
            // InternalPolicyDsl.g:1901:2: ( ruleImport )
            {
            // InternalPolicyDsl.g:1901:2: ( ruleImport )
            // InternalPolicyDsl.g:1902:3: ruleImport
            {
             before(grammarAccess.getPolicyAccess().getImportsImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getPolicyAccess().getImportsImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__ImportsAssignment_0"


    // $ANTLR start "rule__Policy__RoledeclarationAssignment_1"
    // InternalPolicyDsl.g:1911:1: rule__Policy__RoledeclarationAssignment_1 : ( ruleRoleDeclaration ) ;
    public final void rule__Policy__RoledeclarationAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1915:1: ( ( ruleRoleDeclaration ) )
            // InternalPolicyDsl.g:1916:2: ( ruleRoleDeclaration )
            {
            // InternalPolicyDsl.g:1916:2: ( ruleRoleDeclaration )
            // InternalPolicyDsl.g:1917:3: ruleRoleDeclaration
            {
             before(grammarAccess.getPolicyAccess().getRoledeclarationRoleDeclarationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRoleDeclaration();

            state._fsp--;

             after(grammarAccess.getPolicyAccess().getRoledeclarationRoleDeclarationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__RoledeclarationAssignment_1"


    // $ANTLR start "rule__Policy__RulesAssignment_2"
    // InternalPolicyDsl.g:1926:1: rule__Policy__RulesAssignment_2 : ( ruleRule ) ;
    public final void rule__Policy__RulesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1930:1: ( ( ruleRule ) )
            // InternalPolicyDsl.g:1931:2: ( ruleRule )
            {
            // InternalPolicyDsl.g:1931:2: ( ruleRule )
            // InternalPolicyDsl.g:1932:3: ruleRule
            {
             before(grammarAccess.getPolicyAccess().getRulesRuleParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleRule();

            state._fsp--;

             after(grammarAccess.getPolicyAccess().getRulesRuleParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Policy__RulesAssignment_2"


    // $ANTLR start "rule__RoleDeclaration__NameAssignment_1"
    // InternalPolicyDsl.g:1941:1: rule__RoleDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__RoleDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1945:1: ( ( RULE_ID ) )
            // InternalPolicyDsl.g:1946:2: ( RULE_ID )
            {
            // InternalPolicyDsl.g:1946:2: ( RULE_ID )
            // InternalPolicyDsl.g:1947:3: RULE_ID
            {
             before(grammarAccess.getRoleDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRoleDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleDeclaration__NameAssignment_1"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalPolicyDsl.g:1956:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1960:1: ( ( RULE_STRING ) )
            // InternalPolicyDsl.g:1961:2: ( RULE_STRING )
            {
            // InternalPolicyDsl.g:1961:2: ( RULE_STRING )
            // InternalPolicyDsl.g:1962:3: RULE_STRING
            {
             before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__Rule__IdAssignment_1"
    // InternalPolicyDsl.g:1971:1: rule__Rule__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Rule__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1975:1: ( ( RULE_ID ) )
            // InternalPolicyDsl.g:1976:2: ( RULE_ID )
            {
            // InternalPolicyDsl.g:1976:2: ( RULE_ID )
            // InternalPolicyDsl.g:1977:3: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__IdAssignment_1"


    // $ANTLR start "rule__Rule__LhsAssignment_3"
    // InternalPolicyDsl.g:1986:1: rule__Rule__LhsAssignment_3 : ( ruleLHS ) ;
    public final void rule__Rule__LhsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:1990:1: ( ( ruleLHS ) )
            // InternalPolicyDsl.g:1991:2: ( ruleLHS )
            {
            // InternalPolicyDsl.g:1991:2: ( ruleLHS )
            // InternalPolicyDsl.g:1992:3: ruleLHS
            {
             before(grammarAccess.getRuleAccess().getLhsLHSParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLHS();

            state._fsp--;

             after(grammarAccess.getRuleAccess().getLhsLHSParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__LhsAssignment_3"


    // $ANTLR start "rule__Rule__RhsAssignment_6"
    // InternalPolicyDsl.g:2001:1: rule__Rule__RhsAssignment_6 : ( ruleRHS ) ;
    public final void rule__Rule__RhsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2005:1: ( ( ruleRHS ) )
            // InternalPolicyDsl.g:2006:2: ( ruleRHS )
            {
            // InternalPolicyDsl.g:2006:2: ( ruleRHS )
            // InternalPolicyDsl.g:2007:3: ruleRHS
            {
             before(grammarAccess.getRuleAccess().getRhsRHSParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleRHS();

            state._fsp--;

             after(grammarAccess.getRuleAccess().getRhsRHSParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__RhsAssignment_6"


    // $ANTLR start "rule__LHS__SubjectsAssignment_1"
    // InternalPolicyDsl.g:2016:1: rule__LHS__SubjectsAssignment_1 : ( ruleSubject ) ;
    public final void rule__LHS__SubjectsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2020:1: ( ( ruleSubject ) )
            // InternalPolicyDsl.g:2021:2: ( ruleSubject )
            {
            // InternalPolicyDsl.g:2021:2: ( ruleSubject )
            // InternalPolicyDsl.g:2022:3: ruleSubject
            {
             before(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSubject();

            state._fsp--;

             after(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__SubjectsAssignment_1"


    // $ANTLR start "rule__LHS__SubjectsAssignment_2_1"
    // InternalPolicyDsl.g:2031:1: rule__LHS__SubjectsAssignment_2_1 : ( ruleSubject ) ;
    public final void rule__LHS__SubjectsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2035:1: ( ( ruleSubject ) )
            // InternalPolicyDsl.g:2036:2: ( ruleSubject )
            {
            // InternalPolicyDsl.g:2036:2: ( ruleSubject )
            // InternalPolicyDsl.g:2037:3: ruleSubject
            {
             before(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSubject();

            state._fsp--;

             after(grammarAccess.getLHSAccess().getSubjectsSubjectParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__SubjectsAssignment_2_1"


    // $ANTLR start "rule__LHS__ActionsAssignment_5"
    // InternalPolicyDsl.g:2046:1: rule__LHS__ActionsAssignment_5 : ( ruleAction ) ;
    public final void rule__LHS__ActionsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2050:1: ( ( ruleAction ) )
            // InternalPolicyDsl.g:2051:2: ( ruleAction )
            {
            // InternalPolicyDsl.g:2051:2: ( ruleAction )
            // InternalPolicyDsl.g:2052:3: ruleAction
            {
             before(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__ActionsAssignment_5"


    // $ANTLR start "rule__LHS__ActionsAssignment_6_1"
    // InternalPolicyDsl.g:2061:1: rule__LHS__ActionsAssignment_6_1 : ( ruleAction ) ;
    public final void rule__LHS__ActionsAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2065:1: ( ( ruleAction ) )
            // InternalPolicyDsl.g:2066:2: ( ruleAction )
            {
            // InternalPolicyDsl.g:2066:2: ( ruleAction )
            // InternalPolicyDsl.g:2067:3: ruleAction
            {
             before(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getLHSAccess().getActionsActionParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__ActionsAssignment_6_1"


    // $ANTLR start "rule__LHS__ObjectConditionAssignment_9"
    // InternalPolicyDsl.g:2076:1: rule__LHS__ObjectConditionAssignment_9 : ( ruleObjectCondition ) ;
    public final void rule__LHS__ObjectConditionAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2080:1: ( ( ruleObjectCondition ) )
            // InternalPolicyDsl.g:2081:2: ( ruleObjectCondition )
            {
            // InternalPolicyDsl.g:2081:2: ( ruleObjectCondition )
            // InternalPolicyDsl.g:2082:3: ruleObjectCondition
            {
             before(grammarAccess.getLHSAccess().getObjectConditionObjectConditionParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleObjectCondition();

            state._fsp--;

             after(grammarAccess.getLHSAccess().getObjectConditionObjectConditionParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LHS__ObjectConditionAssignment_9"


    // $ANTLR start "rule__RHS__DecisionsAssignment"
    // InternalPolicyDsl.g:2091:1: rule__RHS__DecisionsAssignment : ( ruleAccessDecision ) ;
    public final void rule__RHS__DecisionsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2095:1: ( ( ruleAccessDecision ) )
            // InternalPolicyDsl.g:2096:2: ( ruleAccessDecision )
            {
            // InternalPolicyDsl.g:2096:2: ( ruleAccessDecision )
            // InternalPolicyDsl.g:2097:3: ruleAccessDecision
            {
             before(grammarAccess.getRHSAccess().getDecisionsAccessDecisionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAccessDecision();

            state._fsp--;

             after(grammarAccess.getRHSAccess().getDecisionsAccessDecisionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RHS__DecisionsAssignment"


    // $ANTLR start "rule__Subject__NameAssignment_1"
    // InternalPolicyDsl.g:2106:1: rule__Subject__NameAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Subject__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2110:1: ( ( ( RULE_ID ) ) )
            // InternalPolicyDsl.g:2111:2: ( ( RULE_ID ) )
            {
            // InternalPolicyDsl.g:2111:2: ( ( RULE_ID ) )
            // InternalPolicyDsl.g:2112:3: ( RULE_ID )
            {
             before(grammarAccess.getSubjectAccess().getNameRoleDeclarationCrossReference_1_0()); 
            // InternalPolicyDsl.g:2113:3: ( RULE_ID )
            // InternalPolicyDsl.g:2114:4: RULE_ID
            {
             before(grammarAccess.getSubjectAccess().getNameRoleDeclarationIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSubjectAccess().getNameRoleDeclarationIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getSubjectAccess().getNameRoleDeclarationCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subject__NameAssignment_1"


    // $ANTLR start "rule__Action__ActionAssignment_1"
    // InternalPolicyDsl.g:2125:1: rule__Action__ActionAssignment_1 : ( ruleActionKind ) ;
    public final void rule__Action__ActionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2129:1: ( ( ruleActionKind ) )
            // InternalPolicyDsl.g:2130:2: ( ruleActionKind )
            {
            // InternalPolicyDsl.g:2130:2: ( ruleActionKind )
            // InternalPolicyDsl.g:2131:3: ruleActionKind
            {
             before(grammarAccess.getActionAccess().getActionActionKindEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleActionKind();

            state._fsp--;

             after(grammarAccess.getActionAccess().getActionActionKindEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__ActionAssignment_1"


    // $ANTLR start "rule__ClassCondition__ClassCondRefAssignment_1"
    // InternalPolicyDsl.g:2140:1: rule__ClassCondition__ClassCondRefAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__ClassCondition__ClassCondRefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2144:1: ( ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:2145:2: ( ( ruleFQN ) )
            {
            // InternalPolicyDsl.g:2145:2: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:2146:3: ( ruleFQN )
            {
             before(grammarAccess.getClassConditionAccess().getClassCondRefEClassCrossReference_1_0()); 
            // InternalPolicyDsl.g:2147:3: ( ruleFQN )
            // InternalPolicyDsl.g:2148:4: ruleFQN
            {
             before(grammarAccess.getClassConditionAccess().getClassCondRefEClassFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getClassConditionAccess().getClassCondRefEClassFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getClassConditionAccess().getClassCondRefEClassCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__ClassCondRefAssignment_1"


    // $ANTLR start "rule__ClassCondition__ValueAssignment_2_3"
    // InternalPolicyDsl.g:2159:1: rule__ClassCondition__ValueAssignment_2_3 : ( RULE_STRING ) ;
    public final void rule__ClassCondition__ValueAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2163:1: ( ( RULE_STRING ) )
            // InternalPolicyDsl.g:2164:2: ( RULE_STRING )
            {
            // InternalPolicyDsl.g:2164:2: ( RULE_STRING )
            // InternalPolicyDsl.g:2165:3: RULE_STRING
            {
             before(grammarAccess.getClassConditionAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getClassConditionAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassCondition__ValueAssignment_2_3"


    // $ANTLR start "rule__ReferenceCondition__FeatureCondRefAssignment_1"
    // InternalPolicyDsl.g:2174:1: rule__ReferenceCondition__FeatureCondRefAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__ReferenceCondition__FeatureCondRefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2178:1: ( ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:2179:2: ( ( ruleFQN ) )
            {
            // InternalPolicyDsl.g:2179:2: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:2180:3: ( ruleFQN )
            {
             before(grammarAccess.getReferenceConditionAccess().getFeatureCondRefEReferenceCrossReference_1_0()); 
            // InternalPolicyDsl.g:2181:3: ( ruleFQN )
            // InternalPolicyDsl.g:2182:4: ruleFQN
            {
             before(grammarAccess.getReferenceConditionAccess().getFeatureCondRefEReferenceFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getReferenceConditionAccess().getFeatureCondRefEReferenceFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getReferenceConditionAccess().getFeatureCondRefEReferenceCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceCondition__FeatureCondRefAssignment_1"


    // $ANTLR start "rule__FeatureCondition__FeatureCondRefAssignment_1"
    // InternalPolicyDsl.g:2193:1: rule__FeatureCondition__FeatureCondRefAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__FeatureCondition__FeatureCondRefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2197:1: ( ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:2198:2: ( ( ruleFQN ) )
            {
            // InternalPolicyDsl.g:2198:2: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:2199:3: ( ruleFQN )
            {
             before(grammarAccess.getFeatureConditionAccess().getFeatureCondRefEAttributeCrossReference_1_0()); 
            // InternalPolicyDsl.g:2200:3: ( ruleFQN )
            // InternalPolicyDsl.g:2201:4: ruleFQN
            {
             before(grammarAccess.getFeatureConditionAccess().getFeatureCondRefEAttributeFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getFeatureConditionAccess().getFeatureCondRefEAttributeFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getFeatureConditionAccess().getFeatureCondRefEAttributeCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureCondition__FeatureCondRefAssignment_1"


    // $ANTLR start "rule__OperationCondition__OperationCondRefAssignment_1"
    // InternalPolicyDsl.g:2212:1: rule__OperationCondition__OperationCondRefAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__OperationCondition__OperationCondRefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2216:1: ( ( ( ruleFQN ) ) )
            // InternalPolicyDsl.g:2217:2: ( ( ruleFQN ) )
            {
            // InternalPolicyDsl.g:2217:2: ( ( ruleFQN ) )
            // InternalPolicyDsl.g:2218:3: ( ruleFQN )
            {
             before(grammarAccess.getOperationConditionAccess().getOperationCondRefEOperationCrossReference_1_0()); 
            // InternalPolicyDsl.g:2219:3: ( ruleFQN )
            // InternalPolicyDsl.g:2220:4: ruleFQN
            {
             before(grammarAccess.getOperationConditionAccess().getOperationCondRefEOperationFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getOperationConditionAccess().getOperationCondRefEOperationFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getOperationConditionAccess().getOperationCondRefEOperationCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperationCondition__OperationCondRefAssignment_1"


    // $ANTLR start "rule__AccessDecision__DecisionAssignment"
    // InternalPolicyDsl.g:2231:1: rule__AccessDecision__DecisionAssignment : ( ruleDecisionKind ) ;
    public final void rule__AccessDecision__DecisionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPolicyDsl.g:2235:1: ( ( ruleDecisionKind ) )
            // InternalPolicyDsl.g:2236:2: ( ruleDecisionKind )
            {
            // InternalPolicyDsl.g:2236:2: ( ruleDecisionKind )
            // InternalPolicyDsl.g:2237:3: ruleDecisionKind
            {
             before(grammarAccess.getAccessDecisionAccess().getDecisionDecisionKindEnumRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDecisionKind();

            state._fsp--;

             after(grammarAccess.getAccessDecisionAccess().getDecisionDecisionKindEnumRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AccessDecision__DecisionAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000009000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000003800L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000E10000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000001000000002L});

}